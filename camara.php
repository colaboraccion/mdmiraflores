<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">Camaras Demostrativa</div>
			<div id="m_buscador" class="panel-body">
				<div class="row">
					<div class="col-lg-6">
						<div class="row">
							<div class="col-lg-6">
								<div class="panel panel-default" style="margin-bottom:0px">
									<div class="panel-heading">Cámara 1 fija</div>
									<div class="panel-body" style="padding:0px">
										<iframe src="http://200.37.252.189:2000/172.16.5.2" style="width:170px; height:140px" frameborder="0" scrolling="no"></iframe>
									</div>
								</div>
								<div class="panel panel-default" style="margin-bottom:0px">
									<div class="panel-heading">Cámara 2 fija</div>
									<div class="panel-body" style="padding:0px">
										<iframe src="http://200.37.252.189:2000/172.16.5.6" style="width:170px; height:140px" frameborder="0" scrolling="no"></iframe>
									</div>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="panel panel-default" style="margin-bottom:0px">
									<div class="panel-heading">Cámara 3 fija</div>
									<div class="panel-body" style="padding:0px">
										<iframe src="http://200.37.252.189:2000/172.16.5.1" style="width:170px; height:140px" frameborder="0" scrolling="no"></iframe>
									</div>
								</div>
								<div class="panel panel-default" style="margin-bottom:0px">
									<div class="panel-heading">Cámara 4 fija</div>
									<div class="panel-body" style="padding:0px">
										<iframe src="http://200.37.252.189:2000/172.16.5.10" style="width:170px; height:140px" frameborder="0" scrolling="no"></iframe>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-5">
						<div class="panel panel-default" style="margin-bottom:0px">
							<div class="panel-heading">Cámara Domo PTZ</div>
							<div class="panel-body" style="padding:0px">
								<iframe src="http://200.37.252.189:2000/172.16.5.5" style="width:329px; height:250px" frameborder="0" scrolling="no"></iframe>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<table class="table table-hover">
							<tbody>
								<tr>
									<td>Cámara Fija 1</td>
									<td>Cámara Fija 2</td>
									<td>Cámara Domo PTZ</td>
								</tr>
								<tr>
									<td>
										<iframe src="http://200.37.252.189:2000/172.16.5.2" style="width:170px; height:140px" frameborder="0" scrolling="no"></iframe>
										<iframe src="http://200.37.252.189:2000/172.16.5.6" style="width:170px; height:140px" frameborder="0" scrolling="no"></iframe>
									</td>
									<td>
										<!-- <iframe src="http://200.37.252.189:2000/172.16.5.6" style="width:170px; height:140px" frameborder="0" scrolling="no"></iframe> -->
									</td>
									<td>
										<iframe src="http://200.37.252.189:2000/172.16.5.5" style="width:170px; height:140px" frameborder="0" scrolling="no"></iframe>
									</td>
								</tr>
								<tr>
									<td>
										<iframe src="http://200.37.252.189:2000/172.16.5.10" style="width:170px; height:140px" frameborder="0" scrolling="no"></iframe>
									</td>
									<td>
										<iframe src="http://200.37.252.189:2000/172.16.5.8" style="width:170px; height:140px" frameborder="0" scrolling="no"></iframe>
									</td>
								</tr>
								<tr>
									<td>Cámara Fija 3</td>
									<td>Cámara Fija 4</td>
									<td></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>