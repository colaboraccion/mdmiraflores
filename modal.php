<?php
require_once 'php/app.php';
$fs_analisis_camaras = call_function((object) ['method' => 'fs_analisis_camaras']);
?>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <!--<h4 class="modal-title">Análisis de video vigilancia</h4>-->
		<h4 class="modal-title">Reportes</h4>
      </div>
      <div class="modal-body">
          <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#home">Provincia/Distrito</a></li>
            <li><a data-toggle="tab" href="#menu1">Vías</a></li>
            <li><a data-toggle="tab" href="#menu2">Operador</a></li>
          </ul>

          <div class="tab-content">
            <div id="home" class="tab-pane fade in active">
              <!-- -->
              <div class="row">
                  <div class="col-lg-12">
					  <div id="container1"  class="text-center" style="height: 300px;"><img src="img/200.gif"/></div>
                      <table id="tblDetUbigeo" class="table">
                          <thead>
                              <tr>
                                  <td></td>
                                  <td>Provincia</td>
                                  <td>Muy alto</td>
                                  <td>Alto</td>
                                  <td>Medio</td>
                                  <td>Bajo</td>
                                  <td>Muy bajo</td>
                                  <td>Total</td>
                              </tr>
                          </thead>
                          <tbody>
                              <?php $fs_hexagonos_prov = call_function((object) ['method' => 'fs_hexagonos_prov']);?>
                              <?php foreach ($fs_hexagonos_prov as $key_prov) {?>
                                  <tr>
                                      <td><input type="button" class="btn btn-xs btn-toogle" value="+" data-ubigeo="<?php echo $key_prov->codubigeo; ?>" data-nivel="1"/></td>
                                      <td><a href="#" data-cql="idprov='<?php echo $key_prov->codubigeo; ?>'"><?php echo $key_prov->ubigeo; ?></a></td>
                                      <td class="text-right"><a href="#" data-cql="idprov='<?php echo $key_prov->codubigeo; ?>' and clasifica = 5"><?php echo $key_prov->muy_alto; ?></a></td>
                                      <td class="text-right"><a href="#" data-cql="idprov='<?php echo $key_prov->codubigeo; ?>' and clasifica = 4"><?php echo $key_prov->alto; ?></a></td>
                                      <td class="text-right"><a href="#" data-cql="idprov='<?php echo $key_prov->codubigeo; ?>' and clasifica = 3"><?php echo $key_prov->medio; ?></a></td>
                                      <td class="text-right"><a href="#" data-cql="idprov='<?php echo $key_prov->codubigeo; ?>' and clasifica = 2"><?php echo $key_prov->bajo; ?></a></td>
                                      <td class="text-right"><a href="#" data-cql="idprov='<?php echo $key_prov->codubigeo; ?>' and clasifica = 1"><?php echo $key_prov->muy_bajo; ?></a></td>
                                      <td class="text-right"><a href="#" data-cql="idprov='<?php echo $key_prov->codubigeo; ?>'"><?php echo $key_prov->total; ?></a></td>
                                  </tr>
                                  <tr class="hide">
                                      <td colspan="8">
                                          <table id="tbl_<?php echo $key_prov->codubigeo; ?>" class="table">
                                              <thead>
                                                  <tr>
                                                      <td></td>
                                                      <td>Distrito</td>
                                                      <td>Muy alto</td>
                                                      <td>Alto</td>
                                                      <td>Medio</td>
                                                      <td>Bajo</td>
                                                      <td>Muy bajo</td>
                                                      <td>Total</td>
                                                  </tr>
                                              </thead>
                                              <tbody></tbody>
                                          </table>
                                      </td>
                                  </tr>
                              <?php }?>
                          </tbody>
                      </table>
                  </div>
              </div>
              <!-- -->
            </div>
            <div id="menu1" class="tab-pane fade">
				<div class="row">
					<div class="col-lg-12">
					    <div id="container2" class="text-center" style="height: 300px;"><img src="img/200.gif"/></div>
						<table id="tblDetVia" class="table">
							<thead>
								<tr>
									<td></td>
									<td>Vía</td>
									<td>Muy alto</td>
									<td>Alto</td>
									<td>Medio</td>
									<td>Bajo</td>
									<td>Muy bajo</td>
									<td>Total</td>
								</tr>
							</thead>
							<tbody>
								<?php $fs_tmp = call_function((object) ['method' => 'fs_vias']);
$fs_vias = $fs_tmp;unset($fs_vias[4]);unset($fs_vias[5]);?>
								<?php foreach ($fs_vias as $key_via) {?>
									<tr>
										<td><input type="button" class="btn btn-xs btn-toogle" value="+" data-codvia="<?php echo $key_via->cod_via; ?>"/></td>
										<td><a href="#" data-cql="<?php echo $key_via->cod_via; ?> > 0"><?php echo $key_via->via; ?></a></td>
										<td class="text-right"><a href="#" data-cql="clasifica = 5 and <?php echo $key_via->cod_via; ?> > 0"><?php echo $key_via->muy_alto; ?></a></td>
										<td class="text-right"><a href="#" data-cql="clasifica = 4 and <?php echo $key_via->cod_via; ?> > 0"><?php echo $key_via->alto; ?></a></td>
										<td class="text-right"><a href="#" data-cql="clasifica = 3 and <?php echo $key_via->cod_via; ?> > 0"><?php echo $key_via->medio; ?></a></td>
										<td class="text-right"><a href="#" data-cql="clasifica = 2 and <?php echo $key_via->cod_via; ?> > 0"><?php echo $key_via->bajo; ?></a></td>
										<td class="text-right"><a href="#" data-cql="clasifica = 1 and <?php echo $key_via->cod_via; ?> > 0"><?php echo $key_via->muy_bajo; ?></a></td>
										<td class="text-right"><a href="#" data-cql="<?php echo $key_via->cod_via; ?> > 1"><?php echo $key_via->total; ?></a></td>
									</tr>
									<tr class="hide">
										<td colspan="8">
											<table id="tbl_<?php echo $key_via->cod_via; ?>" class="table">
												<thead>
													<tr>
														<td>Distrito</td>
														<td>Muy alto</td>
														<td>Alto</td>
														<td>Medio</td>
														<td>Bajo</td>
														<td>Muy bajo</td>
														<td>Total</td>
													</tr>
												</thead>
												<tbody></tbody>
											</table>
										</td>
									</tr>
								<?php }?>
							</tbody>
						</table>
					</div>
				</div>
            </div>
            <div id="menu2" class="tab-pane fade">
              <div class="row">
					<div class="col-lg-12">
					    <div id="container3" class="text-center" style="height: 300px;"><img src="img/200.gif"/></div>
						<table id="tblDetVia" class="table">
							<thead>
								<tr>
									<td></td>
									<td>Vía</td>
									<td>Muy alto</td>
									<td>Alto</td>
									<td>Medio</td>
									<td>Bajo</td>
									<td>Muy bajo</td>
									<td>Total</td>
								</tr>
							</thead>
							<tbody>
								<?php
$fs_vias = $fs_tmp;unset($fs_vias[0]);unset($fs_vias[1]);unset($fs_vias[2]);unset($fs_vias[3]);
foreach ($fs_vias as $key_via) {?>
									<tr>
										<td><input type="button" class="btn btn-xs btn-toogle" value="+" data-codvia="<?php echo $key_via->cod_via; ?>"/></td>
										<td><a href="#" data-cql="<?php echo $key_via->cod_via; ?> > 0"><?php echo $key_via->via; ?></a></td>
										<td class="text-right"><a href="#" data-cql="clasifica = 5 and <?php echo $key_via->cod_via; ?> > 0"><?php echo $key_via->muy_alto; ?></a></td>
										<td class="text-right"><a href="#" data-cql="clasifica = 4 and <?php echo $key_via->cod_via; ?> > 0"><?php echo $key_via->alto; ?></a></td>
										<td class="text-right"><a href="#" data-cql="clasifica = 3 and <?php echo $key_via->cod_via; ?> > 0"><?php echo $key_via->medio; ?></a></td>
										<td class="text-right"><a href="#" data-cql="clasifica = 2 and <?php echo $key_via->cod_via; ?> > 0"><?php echo $key_via->bajo; ?></a></td>
										<td class="text-right"><a href="#" data-cql="clasifica = 1 and <?php echo $key_via->cod_via; ?> > 0"><?php echo $key_via->muy_bajo; ?></a></td>
										<td class="text-right"><a href="#" data-cql="<?php echo $key_via->cod_via; ?> > 1"><?php echo $key_via->total; ?></a></td>
									</tr>
									<tr class="hide">
										<td colspan="8">
											<table id="tbl_<?php echo $key_via->cod_via; ?>" class="table">
												<thead>
													<tr>
														<td>Distrito</td>
														<td>Muy alto</td>
														<td>Alto</td>
														<td>Medio</td>
														<td>Bajo</td>
														<td>Muy bajo</td>
														<td>Total</td>
													</tr>
												</thead>
												<tbody></tbody>
											</table>
										</td>
									</tr>
								<?php }?>
							</tbody>
						</table>
					</div>
				</div>
            </div>
          </div>
        <table border="1" style="display: none;">
            <tr>
                <td>Código</td>
                <td>Distrito</td>
                <td>Población</td>
                <td>Vivienda</td>
                <td>Area Km2</td>
                <td>Cámaras instaladas</td>
                <td>Estimación de cámaras en Distritos</td>
                <td>Brecha de Cámaras en Distritos</td>
                <td>Estimación de Cámaras en vías MML</td>
                <td>Brecha de Cámaras en vías MML</td>
                <td>Total Puntos de Videovigilancia</td>
                <td>Presupuesto</td>
            </tr>
            <?php foreach ($fs_analisis_camaras as $key) {?>
                <tr>
                <td><?php echo $key->iddist; ?></td>
                <td><?php echo $key->distrito; ?></td>
                <td class="text-right"><?php echo number_format($key->poblacion, 0, '.', ','); ?></td>
                <td class="text-right"><?php echo number_format($key->vivienda, 0, '.', ','); ?></td>
                <td class="text-right"><?php echo $key->area_km2; ?></td>
                <td class="text-right"><?php echo $key->camaras; ?></td>
                <td class="text-right"><?php echo $key->est_cam_dist; ?></td>
                <td class="text-right"><?php echo empty($key->brecha_cam_dist) ? '-' : $key->brecha_cam_dist; ?></td>
                <td class="text-right"><?php echo $key->est_cam_dist; ?></td>
                <td class="text-right"><?php echo $key->brecha_cam_vias; ?></td>
                <td class="text-right"><?php echo $key->totalpuntos; ?></td>
                <td class="text-right"><?php echo number_format($key->presupuesto, 0, '.', ','); ?></td>
            </tr>
            <?php }?>
            <tr>
                <td></td>
                <td class="text-center">Total</td>
                <td align="right"><?php echo number_format(array_sum(array_column($fs_analisis_camaras, 'poblacion')), 0, '.', ','); ?></td>
                <td align="right"><?php echo number_format(array_sum(array_column($fs_analisis_camaras, 'vivienda')), 0, '.', ','); ?></td>
                <td align="right"><?php echo number_format(array_sum(array_column($fs_analisis_camaras, 'area_km2')), 0, '.', ','); ?></td>
                <td align="right"><?php echo number_format(array_sum(array_column($fs_analisis_camaras, 'camaras')), 0, '.', ','); ?></td>
                <td align="right"><?php echo number_format(array_sum(array_column($fs_analisis_camaras, 'est_cam_dist')), 0, '.', ','); ?></td>
                <td align="right"><?php echo number_format(array_sum(array_column($fs_analisis_camaras, 'brecha_cam_dist')), 0, '.', ','); ?></td>
                <td align="right"><?php echo number_format(array_sum(array_column($fs_analisis_camaras, 'est_cam_dist')), 0, '.', ','); ?></td>
                <td align="right"><?php echo number_format(array_sum(array_column($fs_analisis_camaras, 'brecha_cam_vias')), 0, '.', ','); ?></td>
                <td align="right"><?php echo number_format(array_sum(array_column($fs_analisis_camaras, 'totalpuntos')), 0, '.', ','); ?></td>
                <td align="right"><?php echo number_format(array_sum(array_column($fs_analisis_camaras, 'presupuesto')), 0, '.', ','); ?></td>

            </tr>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>
