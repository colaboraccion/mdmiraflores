<?php
require 'PgSql.php';
ini_set('max_execution_time', 300);

if (count($_GET) > 0) {
    if (isset($_GET['data'])) {
        $params = json_decode($_GET['data']);

        if (isset($params->method)) {
            echo json_encode(call_function($params));
        }
    }
}

function call_function($params)
{
    $data = array();
    switch ($params->method) {
        case 'fs_dist':
            $pg = new PgSql();
            $data = $pg->getRows("select iddist, distrito from distrito_f where substr(idprov,1,4) = '" . $params->codprov . "' order by 2");
            break;
        case 'fs_hexagonos_prov':
            $pg = new PgSql();
            $data = $pg->getRows("
            select
            x.*,
            muy_bajo + bajo + medio + alto + muy_alto as total
            from(
            select
            first_idpr as codubigeo,
            nombprov as ubigeo,
            SUM( CASE WHEN clasifica = 1 THEN 1 ELSE 0 END )  as muy_bajo,
            SUM( CASE WHEN clasifica = 2 THEN 1 ELSE 0 END )  as bajo,
            SUM( CASE WHEN clasifica = 3 THEN 1 ELSE 0 END )  as medio,
            SUM( CASE WHEN clasifica = 4 THEN 1 ELSE 0 END )  as alto,
            SUM( CASE WHEN clasifica = 5 THEN 1 ELSE 0 END )  as muy_alto
            from provincia p
            inner join capas_base.tmp_hex_lima_callao_20201015_1_copia h on substr(h.iddist,1,4) = p.first_idpr
            where
            first_idpr in('0701', '1501')
            group by
            first_idpr,
            nombprov
            order by
            2
            ) x");
            //$data = $pg->getRows("select * from capas_base.tmp_hex_prov");
            /*$data = $pg->getRows("select  id as codubigeo, 'Provincia' as ubigeo, 0 as muy_alto, 0 as alto, 0 as medio, 0 as bajo, 0 as muy_bajo, 0 as total from generate_series(1,4) as id");*/
            break;
        case 'fs_hexagonos_dist':
            $pg = new PgSql();
            $data = $pg->getRows("
            select
            x.*,
            muy_bajo + bajo + medio + alto + muy_alto as total
            from(
            select
            d.iddist as codubigeo,
            nombdist as ubigeo,
            SUM( CASE WHEN clasifica = 1 THEN 1 ELSE 0 END )  as muy_bajo,
            SUM( CASE WHEN clasifica = 2 THEN 1 ELSE 0 END )  as bajo,
            SUM( CASE WHEN clasifica = 3 THEN 1 ELSE 0 END )  as medio,
            SUM( CASE WHEN clasifica = 4 THEN 1 ELSE 0 END )  as alto,
            SUM( CASE WHEN clasifica = 5 THEN 1 ELSE 0 END )  as muy_alto
            from distrito d
            inner join capas_base.tmp_hex_lima_callao_20201015_1_copia h on h.iddist = d.iddist
            where
            d.idprov = '" . $params->codubigeo . "'
            group by
            d.iddist,
            nombdist
            order by
            2
            ) x");
            //$data = $pg->getRows("select * from capas_base.tmp_hex_dist where substring(codubigeo, 1,4) = '" . $params->codubigeo . "'");
            /*$data = $pg->getRows("select  concat('d', id) as codubigeo, 'Distrito' as ubigeo, 0 as muy_alto, 0 as alto, 0 as medio, 0 as bajo, 0 as muy_bajo, 0 as total from generate_series(1,4) as id
            cross join(
            select * from generate_series(1,4) as id_via
            ) x");*/
            break;
        case 'fs_hexagonos_vias':
            $pg = new PgSql();
            //$data = $pg->getRows("select  id as codubigeo, 'Vía' as via, 0 as muy_alto, 0 as alto, 0 as medio, 0 as bajo, 0 as muy_bajo, 0 as total from generate_series(1,4) as id");
            //$data = $pg->getRows("select * from capas_base.tmp_hex_vias where codubigeo = '" . $params->codubigeo . "'");
            $data = $pg->getRows("select
            x.*,
            muy_bajo + bajo + medio + alto + muy_alto as total
            from(
            select
            case
            when id_via = 1 then 'cvnacional'
            when id_via = 2 then 'cvmetro'
            when id_via = 3 then 'cvarterial'
            when id_via = 4 then 'cvcolector'
            end as cod_via,
            case
            when id_via = 1 then 'Nacional'
            when id_via = 2 then 'Metropolitana'
            when id_via = 3 then 'Arterial'
            when id_via = 4 then 'Colectora'
            end as via,
            case
            when id_via = 1 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where iddist = '" . $params->codubigeo . "' and cvnacional = 1 and clasifica = 1)
            when id_via = 2 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where iddist = '" . $params->codubigeo . "' and cvmetro = 1 and clasifica = 1)
            when id_via = 3 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where iddist = '" . $params->codubigeo . "' and cvarterial = 1 and clasifica = 1)
            when id_via = 4 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where iddist = '" . $params->codubigeo . "' and cvcolector = 1 and clasifica = 2)
            end as muy_bajo,
            case
            when id_via = 1 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where iddist = '" . $params->codubigeo . "' and cvnacional = 1 and clasifica = 2)
            when id_via = 2 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where iddist = '" . $params->codubigeo . "' and cvmetro = 1 and clasifica = 2)
            when id_via = 3 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where iddist = '" . $params->codubigeo . "' and cvarterial = 1 and clasifica = 2)
            when id_via = 4 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where iddist = '" . $params->codubigeo . "' and cvcolector = 1 and clasifica = 2)
            end as bajo,
            case
            when id_via = 1 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where iddist = '" . $params->codubigeo . "' and cvnacional = 1 and clasifica = 3)
            when id_via = 2 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where iddist = '" . $params->codubigeo . "' and cvmetro = 1 and clasifica = 3)
            when id_via = 3 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where iddist = '" . $params->codubigeo . "' and cvarterial = 1 and clasifica = 3)
            when id_via = 4 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where iddist = '" . $params->codubigeo . "' and cvcolector = 1 and clasifica = 3)
            end as medio,
            case
            when id_via = 1 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where iddist = '" . $params->codubigeo . "' and cvnacional = 1 and clasifica = 4)
            when id_via = 2 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where iddist = '" . $params->codubigeo . "' and cvmetro = 1 and clasifica = 4)
            when id_via = 3 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where iddist = '" . $params->codubigeo . "' and cvarterial = 1 and clasifica = 4)
            when id_via = 4 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where iddist = '" . $params->codubigeo . "' and cvcolector = 1 and clasifica = 4)
            end as alto,
            case
            when id_via = 1 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where iddist = '" . $params->codubigeo . "' and cvnacional = 1 and clasifica = 5)
            when id_via = 2 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where iddist = '" . $params->codubigeo . "' and cvmetro = 1 and clasifica = 5)
            when id_via = 3 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where iddist = '" . $params->codubigeo . "' and cvarterial = 1 and clasifica = 5)
            when id_via = 4 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where iddist = '" . $params->codubigeo . "' and cvcolector = 1 and clasifica = 5)
            end as muy_alto
            from generate_series(1,4) as id_via
            ) x");
            break;
        case 'hex_x_categoria':
            $pg = new PgSql();
            $query = "select
                distrito as valor,
                SUM( CASE WHEN clasifica = 1 THEN 1 ELSE 0 END )  as muy_bajo,
                SUM( CASE WHEN clasifica = 2 THEN 1 ELSE 0 END )  as bajo,
                SUM( CASE WHEN clasifica = 3 THEN 1 ELSE 0 END )  as medio,
                SUM( CASE WHEN clasifica = 4 THEN 1 ELSE 0 END )  as alto,
                SUM( CASE WHEN clasifica = 5 THEN 1 ELSE 0 END )  as muy_alto
            from distrito_f va
            inner join capas_base.tmp_hex_lima_callao_20201015_1_copia h on st_intersects(h.geom, va.geom)
            where
                va.iddist in (" . $params->ubigeos . ")
            group by
                distrito
            order by
                1";

            $arr = $pg->getRow("select
            row_to_json(x)
           FROM(
            select
             (select row_to_json(t) from(select 'column' as type) t) as chart,
             (select row_to_json(t) from(select 'Total de casos por categoría' as text) t) as title,
             (select row_to_json(t) from(select false as enabled) t) as credits,
             (select Row_to_json(t) FROM(select json_agg(x.valor) AS categories) t) AS \"xAxis\",
             (select row_to_json(t) from (select row_to_json(t) as title from(select 'Cantidad de incidentes' as text) t) t) as \"yAxis\",
             (select row_to_json(t) from(select true as enabled) t) as exporting,
             (
              select
               array_to_json(array_agg(x)) as series
              from(
               select 'Muy alto' as name, JSON_AGG(x.muy_alto) as data, '#8C0000' as color union all
               select 'Alto' as name, JSON_AGG(x.alto) as data, '#D90000' as color union all
               select 'Medio' as name, JSON_AGG(x.medio) as data, '#FF5C26' as color union all
               select 'Bajo' as name, JSON_AGG(x.bajo) as data, '#FF7F00' as color union all
               select 'Muy bajo' as name, JSON_AGG(x.muy_bajo) as data, '#FFC926' as color
              ) x
             )
            from(
             " . $query . "
            ) x
           ) x");

            $data = $arr->row_to_json;
            break;
        case 'vias_nacionales':
            $pg = new PgSql();

            $query = "select
                    concat(va.clavia, ' ', va.nombre) as valor,
                    SUM( CASE WHEN clasifica = 1 THEN 1 ELSE 0 END )  as muy_bajo,
                    SUM( CASE WHEN clasifica = 2 THEN 1 ELSE 0 END )  as bajo,
                    SUM( CASE WHEN clasifica = 3 THEN 1 ELSE 0 END )  as medio,
                    SUM( CASE WHEN clasifica = 4 THEN 1 ELSE 0 END )  as alto,
                    SUM( CASE WHEN clasifica = 5 THEN 1 ELSE 0 END )  as muy_alto
                from capas_base.vias_nacionales va
                inner join capas_base.tmp_hex_lima_callao_20201015_1_copia h on st_intersects(h.geom, va.geom)
                where
                    va.iddist in (" . $params->ubigeos . ")
                group by
                    concat(va.clavia, ' ', va.nombre)
                order by
                    1";

            $arr = $pg->getRow("select
                row_to_json(x)
               FROM(
                select
                 (select row_to_json(t) from(select 'column' as type) t) as chart,
                 (select row_to_json(t) from(select 'Total de casos por vías nacionales' as text) t) as title,
                 (select row_to_json(t) from(select false as enabled) t) as credits,
                 (select Row_to_json(t) FROM(select json_agg(x.valor) AS categories) t) AS \"xAxis\",
                 (select row_to_json(t) from (select row_to_json(t) as title from(select 'Cantidad de incidentes' as text) t) t) as \"yAxis\",
                 (select row_to_json(t) from(select true as enabled) t) as exporting,
                 (
                  select
                   array_to_json(array_agg(x)) as series
                  from(
                   select 'Muy alto' as name, JSON_AGG(x.muy_alto) as data, '#8C0000' as color union all
                   select 'Alto' as name, JSON_AGG(x.alto) as data, '#D90000' as color union all
                   select 'Medio' as name, JSON_AGG(x.medio) as data, '#FF5C26' as color union all
                   select 'Bajo' as name, JSON_AGG(x.bajo) as data, '#FF7F00' as color union all
                   select 'Muy bajo' as name, JSON_AGG(x.muy_bajo) as data, '#FFC926' as color
                  ) x
                 )
                from(
                 " . $query . "
                ) x
               ) x");

            $data = $arr->row_to_json;
            break;
        case 'vias_metropolitanas':
            $pg = new PgSql();

            $query = "select
                        concat(va.clavia, ' ', va.nombre) as valor,
                        SUM( CASE WHEN clasifica = 1 THEN 1 ELSE 0 END )  as muy_bajo,
                        SUM( CASE WHEN clasifica = 2 THEN 1 ELSE 0 END )  as bajo,
                        SUM( CASE WHEN clasifica = 3 THEN 1 ELSE 0 END )  as medio,
                        SUM( CASE WHEN clasifica = 4 THEN 1 ELSE 0 END )  as alto,
                        SUM( CASE WHEN clasifica = 5 THEN 1 ELSE 0 END )  as muy_alto
                    from capas_base.vias_metropolitanas va
                    inner join capas_base.tmp_hex_lima_callao_20201015_1_copia h on st_intersects(h.geom, va.geom)
                    where
                        va.iddist in (" . $params->ubigeos . ")
                    group by
                        concat(va.clavia, ' ', va.nombre)
                    order by
                        1";

            $arr = $pg->getRow("select
                    row_to_json(x)
                   FROM(
                    select
                     (select row_to_json(t) from(select 'column' as type) t) as chart,
                     (select row_to_json(t) from(select 'Total de casos por vías metropolitanas' as text) t) as title,
                     (select row_to_json(t) from(select false as enabled) t) as credits,
                     (select Row_to_json(t) FROM(select json_agg(x.valor) AS categories) t) AS \"xAxis\",
                     (select row_to_json(t) from (select row_to_json(t) as title from(select 'Cantidad de incidentes' as text) t) t) as \"yAxis\",
                     (select row_to_json(t) from(select true as enabled) t) as exporting,
                     (
                      select
                       array_to_json(array_agg(x)) as series
                      from(
                       select 'Muy alto' as name, JSON_AGG(x.muy_alto) as data, '#8C0000' as color union all
                       select 'Alto' as name, JSON_AGG(x.alto) as data, '#D90000' as color union all
                       select 'Medio' as name, JSON_AGG(x.medio) as data, '#FF5C26' as color union all
                       select 'Bajo' as name, JSON_AGG(x.bajo) as data, '#FF7F00' as color union all
                       select 'Muy bajo' as name, JSON_AGG(x.muy_bajo) as data, '#FFC926' as color
                      ) x
                     )
                    from(
                     " . $query . "
                    ) x
                   ) x");

            $data = $arr->row_to_json;
            break;
        case 'vias_arteriales':
            $pg = new PgSql();

            $query = "select
                            concat(va.clavia, ' ', va.nombre) as valor,
                            SUM( CASE WHEN clasifica = 1 THEN 1 ELSE 0 END )  as muy_bajo,
                            SUM( CASE WHEN clasifica = 2 THEN 1 ELSE 0 END )  as bajo,
                            SUM( CASE WHEN clasifica = 3 THEN 1 ELSE 0 END )  as medio,
                            SUM( CASE WHEN clasifica = 4 THEN 1 ELSE 0 END )  as alto,
                            SUM( CASE WHEN clasifica = 5 THEN 1 ELSE 0 END )  as muy_alto
                        from capas_base.vias_arteriales va
                        inner join capas_base.tmp_hex_lima_callao_20201015_1_copia h on st_intersects(h.geom, va.geom)
                        where
                            va.iddist in (" . $params->ubigeos . ")
                        group by
                            concat(va.clavia, ' ', va.nombre)
                        order by
                            1";

            $arr = $pg->getRow("select
                        row_to_json(x)
                       FROM(
                        select
                         (select row_to_json(t) from(select 'column' as type) t) as chart,
                         (select row_to_json(t) from(select 'Total de casos por vías arteriales' as text) t) as title,
                         (select row_to_json(t) from(select false as enabled) t) as credits,
                         (select Row_to_json(t) FROM(select json_agg(x.valor) AS categories) t) AS \"xAxis\",
                         (select row_to_json(t) from (select row_to_json(t) as title from(select 'Cantidad de incidentes' as text) t) t) as \"yAxis\",
                         (select row_to_json(t) from(select true as enabled) t) as exporting,
                         (
                          select
                           array_to_json(array_agg(x)) as series
                          from(
                           select 'Muy alto' as name, JSON_AGG(x.muy_alto) as data, '#8C0000' as color union all
                           select 'Alto' as name, JSON_AGG(x.alto) as data, '#D90000' as color union all
                           select 'Medio' as name, JSON_AGG(x.medio) as data, '#FF5C26' as color union all
                           select 'Bajo' as name, JSON_AGG(x.bajo) as data, '#FF7F00' as color union all
                           select 'Muy bajo' as name, JSON_AGG(x.muy_bajo) as data, '#FFC926' as color
                          ) x
                         )
                        from(
                         " . $query . "
                        ) x
                       ) x");

            $data = $arr->row_to_json;
            break;
        case 'vias_colectoras':
            $pg = new PgSql();

            $query = "select
                                concat(va.clavia, ' ', va.nombre) as valor,
                                SUM( CASE WHEN clasifica = 1 THEN 1 ELSE 0 END )  as muy_bajo,
                                SUM( CASE WHEN clasifica = 2 THEN 1 ELSE 0 END )  as bajo,
                                SUM( CASE WHEN clasifica = 3 THEN 1 ELSE 0 END )  as medio,
                                SUM( CASE WHEN clasifica = 4 THEN 1 ELSE 0 END )  as alto,
                                SUM( CASE WHEN clasifica = 5 THEN 1 ELSE 0 END )  as muy_alto
                            from capas_base.vias_colectoras va
                            inner join capas_base.tmp_hex_lima_callao_20201015_1_copia h on st_intersects(h.geom, va.geom)
                            where
                                va.iddist in (" . $params->ubigeos . ")
                            group by
                                concat(va.clavia, ' ', va.nombre)
                            order by
                                1";

            $arr = $pg->getRow("select
                            row_to_json(x)
                           FROM(
                            select
                             (select row_to_json(t) from(select 'column' as type) t) as chart,
                             (select row_to_json(t) from(select 'Total de casos por vías colectoras' as text) t) as title,
                             (select row_to_json(t) from(select false as enabled) t) as credits,
                             (select Row_to_json(t) FROM(select json_agg(x.valor) AS categories) t) AS \"xAxis\",
                             (select row_to_json(t) from (select row_to_json(t) as title from(select 'Cantidad de incidentes' as text) t) t) as \"yAxis\",
                             (select row_to_json(t) from(select true as enabled) t) as exporting,
                             (
                              select
                               array_to_json(array_agg(x)) as series
                              from(
                               select 'Muy alto' as name, JSON_AGG(x.muy_alto) as data, '#8C0000' as color union all
                               select 'Alto' as name, JSON_AGG(x.alto) as data, '#D90000' as color union all
                               select 'Medio' as name, JSON_AGG(x.medio) as data, '#FF5C26' as color union all
                               select 'Bajo' as name, JSON_AGG(x.bajo) as data, '#FF7F00' as color union all
                               select 'Muy bajo' as name, JSON_AGG(x.muy_bajo) as data, '#FFC926' as color
                              ) x
                             )
                            from(
                             " . $query . "
                            ) x
                           ) x");

            $data = $arr->row_to_json;
            break;
        case 'fs_vias':
            $pg = new PgSql();
            $data = $pg->getRows("select
        x.*,
        muy_bajo + bajo + medio + alto + muy_alto as total
        from(
        select
        case
            when id_via = 1 then 'cvnacional'
            when id_via = 2 then 'cvmetro'
            when id_via = 3 then 'cvarterial'
            when id_via = 4 then 'cvcolector'
            when id_via = 5 then 'ccanalizad'
            when id_via = 6 then 'cfibra'
        end as cod_via,
        case
            when id_via = 1 then 'Nacional'
            when id_via = 2 then 'Metropolitana'
            when id_via = 3 then 'Arterial'
            when id_via = 4 then 'Colectora'
            when id_via = 5 then 'Canalizado'
            when id_via = 6 then 'Fibra'
        end as via,
        case
            when id_via = 1 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cvnacional = 1 and clasifica = 1)
            when id_via = 2 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cvmetro = 1 and clasifica = 1)
            when id_via = 3 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cvarterial = 1 and clasifica = 1)
            when id_via = 4 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cvcolector = 1 and clasifica = 1)
            when id_via = 5 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where ccanalizado = 1 and clasifica = 1)
            when id_via = 6 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cfibra = 1 and clasifica = 1)
        end as muy_bajo,
        case
            when id_via = 1 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cvnacional = 1 and clasifica = 2)
            when id_via = 2 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cvmetro = 1 and clasifica = 2)
            when id_via = 3 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cvarterial = 1 and clasifica = 2)
            when id_via = 4 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cvcolector = 1 and clasifica = 2)
            when id_via = 5 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where ccanalizado = 1 and clasifica = 2)
            when id_via = 6 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cfibra = 1 and clasifica = 2)
        end as bajo,
        case
            when id_via = 1 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cvnacional = 1 and clasifica = 3)
            when id_via = 2 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cvmetro = 1 and clasifica = 3)
            when id_via = 3 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cvarterial = 1 and clasifica = 3)
            when id_via = 4 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cvcolector = 1 and clasifica = 3)
            when id_via = 5 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where ccanalizado = 1 and clasifica = 3)
            when id_via = 6 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cfibra = 1 and clasifica = 3)
        end as medio,
        case
            when id_via = 1 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cvnacional = 1 and clasifica = 4)
            when id_via = 2 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cvmetro = 1 and clasifica = 4)
            when id_via = 3 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cvarterial = 1 and clasifica = 4)
            when id_via = 4 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cvcolector = 1 and clasifica = 4)
            when id_via = 5 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where ccanalizado = 1 and clasifica = 4)
            when id_via = 6 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cfibra = 1 and clasifica = 4)
        end as alto,
        case
            when id_via = 1 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cvnacional = 1 and clasifica = 5)
            when id_via = 2 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cvmetro = 1 and clasifica = 5)
            when id_via = 3 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cvarterial = 1 and clasifica = 5)
            when id_via = 4 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cvcolector = 1 and clasifica = 5)
            when id_via = 5 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where ccanalizado = 1 and clasifica = 5)
            when id_via = 6 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cfibra = 1 and clasifica = 5)
        end as muy_alto
        from generate_series(1,6) as id_via
        ) x");
            break;
        case 'fs_vias_dist':
            $pg = new PgSql();
            $data = $pg->getRows("select
                x.*,
                muy_bajo + bajo + medio + alto + muy_alto as total
                from(select
                d.iddist as codubigeo,
                d.nombdist as ubigeo,
                SUM(CASE WHEN clasifica = 1 THEN 1 ELSE 0 END ) AS muy_bajo,
                SUM(CASE WHEN clasifica = 2 THEN 1 ELSE 0 END ) AS bajo,
                SUM(CASE WHEN clasifica = 3 THEN 1 ELSE 0 END ) AS medio,
                SUM(CASE WHEN clasifica = 4 THEN 1 ELSE 0 END ) AS alto,
                SUM(CASE WHEN clasifica = 5 THEN 1 ELSE 0 END ) AS muy_alto
                from capas_base.tmp_hex_lima_callao_20201015_1_copia h
                inner join distrito d on d.iddist = h.iddist
                where
                " . $params->codvia . "
                group by
                d.iddist,
                d.nombdist
                order by
                2
                ) x");
            break;

        case 'fs_analisis_camaras':
            $pg = new PgSql();
            $data = $pg->getRows("select * from capas_base.tmp_analisis_camaras order by 1");
            break;
        case 'fs_ficha':
            $pg = new PgSql();
            $data = $pg->getRow("select * from capas_base.indicadores_distritos i inner join obs.gen_ficha_cabecera f on f.codubigeo = i.ubigeo where ubigeo = '" . $params->iddist . "'");
            break;
            /**aqui densidad*/
        case 'fs_analisis_det_densidad':
            $pg = new PgSql();
            $data = $pg->getRows("select hora,
            case
            when hora =0 then '00'
            when hora =1 then '01'
            when hora =2 then '02'
            when hora =3 then '03'
            when hora =4 then '04'
            when hora =5 then '05'
            when hora =6 then '06'
            when hora =7 then '07'
            when hora =8 then '08'
            when hora =9 then '09'
            when hora =10 then '10'
            when hora =11 then '11'
            when hora =12 then '12'
            when hora =13 then '13'
            when hora =14 then '14'
            when hora =15 then '15'
            when hora =16 then '16'
            when hora =17 then '17'
            when hora =18 then '18'
            when hora =19 then '19'
            when hora =20 then '20'
            when hora =21 then '21'
            when hora =22 then '22'
            when hora =23 then '23'
            end as horat,
            a.sum d,
            b.sum l,
            c.sum m,
            d.sum x,
            e.sum j,
            f.sum v,
            g.sum s
            from generate_series(0,23) hora left join
            (select hora_s,dia,sum(count) from(
                select 
                case when fecha_s=0 then 'D'
                when fecha_s=1 then 'L'
                when fecha_s=2 then 'M'
                when fecha_s=3 then 'X'
                when fecha_s=4 then 'J'
                when fecha_s=5 then 'V'
                when fecha_s=6 then 'S' end dia,
                fecha_s,hora_s,count(*) from(
                    select fecha_s ,hora_s   from observatorio_v2." . $params->table . " a where gid::integer in (select gid::integer from (
                        select * from observatorio_v2." . $params->tablehex . " h where st_intersects(st_setsrid(h.geom,4326),ST_Buffer(st_setsrid(ST_MakePoint(" . $params->lanlng->lng . "," . $params->lanlng->lat . "),4326)::geography,$params->radio))
                )x group by gid) and a.gid!=''
                )y group by hora_s,fecha_s order by 2,1
                )z  where dia='D' group by hora_s,dia order by 2)a on hora.hora=a.hora_s::integer left join
            (select hora_s,dia,sum(count) from(
                select 
                case when fecha_s=0 then 'D'
                when fecha_s=1 then 'L'
                when fecha_s=2 then 'M'
                when fecha_s=3 then 'X'
                when fecha_s=4 then 'J'
                when fecha_s=5 then 'V'
                when fecha_s=6 then 'S' end dia,
                fecha_s,hora_s,count(*) from(
                    select fecha_s ,hora_s   from observatorio_v2." . $params->table . " a where gid::integer in (select gid::integer from (
                        select * from observatorio_v2." . $params->tablehex . " h where st_intersects(st_setsrid(h.geom,4326),ST_Buffer(st_setsrid(ST_MakePoint(" . $params->lanlng->lng . "," . $params->lanlng->lat . "),4326)::geography,$params->radio))
                )x group by gid) and a.gid!=''
                )y group by hora_s,fecha_s order by 2,1
                )z  where dia='L' group by hora_s,dia order by 2)b on hora.hora=b.hora_s::integer left join
            (select hora_s,dia,sum(count) from(
                select 
                case when fecha_s=0 then 'D'
                when fecha_s=1 then 'L'
                when fecha_s=2 then 'M'
                when fecha_s=3 then 'X'
                when fecha_s=4 then 'J'
                when fecha_s=5 then 'V'
                when fecha_s=6 then 'S' end dia,
                fecha_s,hora_s,count(*) from(
                    select fecha_s ,hora_s   from observatorio_v2." . $params->table . " a where gid::integer in (select gid::integer from (
                        select * from observatorio_v2." . $params->tablehex . " h where st_intersects(st_setsrid(h.geom,4326),ST_Buffer(st_setsrid(ST_MakePoint(" . $params->lanlng->lng . "," . $params->lanlng->lat . "),4326)::geography,$params->radio))
                )x group by gid) and a.gid!=''
                )y group by hora_s,fecha_s order by 2,1
                )z  where dia='M' group by hora_s,dia order by 2)c on hora.hora=c.hora_s::integer left join
            (select hora_s,dia,sum(count) from(
                select 
                case when fecha_s=0 then 'D'
                when fecha_s=1 then 'L'
                when fecha_s=2 then 'M'
                when fecha_s=3 then 'X'
                when fecha_s=4 then 'J'
                when fecha_s=5 then 'V'
                when fecha_s=6 then 'S' end dia,
                fecha_s,hora_s,count(*) from(
                    select fecha_s ,hora_s  from observatorio_v2." . $params->table . " a where gid::integer in (select gid::integer from (
                        select * from observatorio_v2." . $params->tablehex . " h where st_intersects(st_setsrid(h.geom,4326),ST_Buffer(st_setsrid(ST_MakePoint(" . $params->lanlng->lng . "," . $params->lanlng->lat . "),4326)::geography,$params->radio))
                )x group by gid) and a.gid!=''
                )y group by hora_s,fecha_s order by 2,1
                )z  where dia='X' group by hora_s,dia order by 2)d on hora.hora=d.hora_s::integer left join
            (select hora_s,dia,sum(count) from(
                select 
                case when fecha_s=0 then 'D'
                when fecha_s=1 then 'L'
                when fecha_s=2 then 'M'
                when fecha_s=3 then 'X'
                when fecha_s=4 then 'J'
                when fecha_s=5 then 'V'
                when fecha_s=6 then 'S' end dia,
                fecha_s,hora_s,count(*) from(
                    select fecha_s ,hora_s   from observatorio_v2." . $params->table . " a where gid::integer in (select gid::integer from (
                        select * from observatorio_v2." . $params->tablehex . " h where st_intersects(st_setsrid(h.geom,4326),ST_Buffer(st_setsrid(ST_MakePoint(" . $params->lanlng->lng . "," . $params->lanlng->lat . "),4326)::geography,$params->radio))
                )x group by gid) and a.gid!=''
                )y group by hora_s,fecha_s order by 2,1
                )z  where dia='J' group by hora_s,dia order by 2)e on hora.hora=e.hora_s::integer left join
            (select hora_s,dia,sum(count) from(
                select 
                case when fecha_s=0 then 'D'
                when fecha_s=1 then 'L'
                when fecha_s=2 then 'M'
                when fecha_s=3 then 'X'
                when fecha_s=4 then 'J'
                when fecha_s=5 then 'V'
                when fecha_s=6 then 'S' end dia,
                fecha_s,hora_s,count(*) from(
                    select fecha_s ,hora_s  from observatorio_v2." . $params->table . " a where gid::integer in (select gid::integer from (
                        select * from observatorio_v2." . $params->tablehex . " h where st_intersects(st_setsrid(h.geom,4326),ST_Buffer(st_setsrid(ST_MakePoint(" . $params->lanlng->lng . "," . $params->lanlng->lat . "),4326)::geography,$params->radio))
                )x group by gid) and a.gid!=''
                )y group by hora_s,fecha_s order by 2,1
                )z  where dia='V' group by hora_s,dia order by 2)f on hora.hora=f.hora_s::integer left join
            (select hora_s,dia,sum(count) from(
                select 
                case when fecha_s=0 then 'D'
                when fecha_s=1 then 'L'
                when fecha_s=2 then 'M'
                when fecha_s=3 then 'X'
                when fecha_s=4 then 'J'
                when fecha_s=5 then 'V'
                when fecha_s=6 then 'S' end dia,
                fecha_s,hora_s,count(*) from(
                    select fecha_s ,hora_s  from observatorio_v2." . $params->table . " a where gid::integer in (select gid::integer from (
                        select * from observatorio_v2." . $params->tablehex . " h where st_intersects(st_setsrid(h.geom,4326),ST_Buffer(st_setsrid(ST_MakePoint(" . $params->lanlng->lng . "," . $params->lanlng->lat . "),4326)::geography,$params->radio))
                )x group by gid) and a.gid!=''
                )y group by hora_s,fecha_s order by 2,1
                )z  where dia='S' group by hora_s,dia order by 2)g on hora.hora=g.hora_s::integer ");
            // $data= 'x';
            break;

            /**fin de densidad */
        case 'fs_analisis_velo':
            $pg = new PgSql();
            $data = $pg->getRows("select hora,
            case
            when hora =0 then '00'
            when hora =1 then '01'
            when hora =2 then '02'
            when hora =3 then '03'
            when hora =4 then '04'
            when hora =5 then '05'
            when hora =6 then '06'
            when hora =7 then '07'
            when hora =8 then '08'
            when hora =9 then '09'
            when hora =10 then '10'
            when hora =11 then '11'
            when hora =12 then '12'
            when hora =13 then '13'
            when hora =14 then '14'
            when hora =15 then '15'
            when hora =16 then '16'
            when hora =17 then '17'
            when hora =18 then '18'
            when hora =19 then '19'
            when hora =20 then '20'
            when hora =21 then '21'
            when hora =22 then '22'
            when hora =23 then '23'
            when hora =24 then '24'
            end as horat,
            a.velo d,
            b.velo l,
            c.velo m,
            d.velo x,
            e.velo j,
            f.velo v,
            g.velo s
            from generate_series(0,23) hora  left join
            (select z.hora_s, z.fecha_s dia,round(avg(velocidad),0) velo from(
            select 
            y.hora_s,y.fecha_s,y.velocidad
            from (
            
            select a.hora_s, a.fecha_s,a.velocidad::integer from observatorio_v2." . $params->table . " a where gid::integer in (select distinct(gid::integer) from (
            select * from observatorio_v2." . $params->tablehex . " h where st_intersects(h.geom,ST_Buffer(st_setsrid(ST_MakePoint(" . $params->lanlng->lng . "," . $params->lanlng->lat . "),4326)::geography,$params->radio))
            )x group by gid) and a.gid!='' and a.velocidad::integer>=10 and a.velocidad::integer<=100 order by 1,2
            
            )y group by y.hora_s,y.velocidad,y.fecha_s order by 1,2,3
            
            )z where z.fecha_s=0 group by z.hora_s,z.fecha_s
            
            )a on hora.hora=a.hora_s::integer left join
            (select z.hora_s, z.fecha_s dia,round(avg(velocidad),0) velo from(
            select 
            y.hora_s,y.fecha_s,y.velocidad
            from (
            
            select a.hora_s, a.fecha_s,a.velocidad::integer from observatorio_v2." . $params->table . " a where gid::integer in (select distinct(gid::integer) from (
            select * from observatorio_v2." . $params->tablehex . " h where st_intersects(h.geom,ST_Buffer(st_setsrid(ST_MakePoint(" . $params->lanlng->lng . "," . $params->lanlng->lat . "),4326)::geography,$params->radio))
            )x group by gid) and a.gid!='' and a.velocidad::integer>=10 and a.velocidad::integer<=100 order by 1,2
            
            )y group by y.hora_s,y.velocidad,y.fecha_s order by 1,2,3
            
            )z where z.fecha_s=1  group by z.hora_s,z.fecha_s
            
            )b on hora.hora=b.hora_s::integer left join
            (select z.hora_s, z.fecha_s dia,round(avg(velocidad),0) velo from(
            select 
            y.hora_s,y.fecha_s,y.velocidad
            from (
            
            select a.hora_s, a.fecha_s,a.velocidad::integer from observatorio_v2." . $params->table . " a where gid::integer in (select distinct(gid::integer) from (
            select * from observatorio_v2." . $params->tablehex . " h where st_intersects(h.geom,ST_Buffer(st_setsrid(ST_MakePoint(" . $params->lanlng->lng . "," . $params->lanlng->lat . "),4326)::geography,$params->radio))
            )x group by gid) and a.gid!='' and a.velocidad::integer>=10 and a.velocidad::integer<=100 order by 1,2
            
            )y group by y.hora_s,y.velocidad,y.fecha_s order by 1,2,3
            
            )z where z.fecha_s=2  group by z.hora_s,z.fecha_s
            
            )c on hora.hora=c.hora_s::integer left join
            (select z.hora_s, z.fecha_s dia,round(avg(velocidad),0) velo from(
            select 
            y.hora_s,y.fecha_s,y.velocidad
            from (
            
            select a.hora_s, a.fecha_s,a.velocidad::integer from observatorio_v2." . $params->table . " a where gid::integer in (select distinct(gid::integer) from (
            select * from observatorio_v2." . $params->tablehex . " h where st_intersects(h.geom,ST_Buffer(st_setsrid(ST_MakePoint(" . $params->lanlng->lng . "," . $params->lanlng->lat . "),4326)::geography,$params->radio))
            )x group by gid) and a.gid!='' and a.velocidad::integer>=10 and a.velocidad::integer<=100 order by 1,2
            
            )y group by y.hora_s,y.velocidad,y.fecha_s order by 1,2,3
            
            )z where z.fecha_s=3  group by z.hora_s,z.fecha_s
            
            )d on hora.hora=d.hora_s::integer left join
            (select z.hora_s, z.fecha_s dia,round(avg(velocidad),0) velo from(
            select 
            y.hora_s,y.fecha_s,y.velocidad
            from (
            
            select a.hora_s, a.fecha_s,a.velocidad::integer from observatorio_v2." . $params->table . " a where gid::integer in (select distinct(gid::integer) from (
            select * from observatorio_v2." . $params->tablehex . " h where st_intersects(h.geom,ST_Buffer(st_setsrid(ST_MakePoint(" . $params->lanlng->lng . "," . $params->lanlng->lat . "),4326)::geography,$params->radio))
            )x group by gid) and a.gid!='' and a.velocidad::integer>=10 and a.velocidad::integer<=100 order by 1,2
            
            )y group by y.hora_s,y.velocidad,y.fecha_s order by 1,2,3
            
            )z where z.fecha_s=4  group by z.hora_s,z.fecha_s
            
            )e on hora.hora=e.hora_s::integer left join
            (select z.hora_s, z.fecha_s dia,round(avg(velocidad),0) velo from(
            select 
            y.hora_s,y.fecha_s,y.velocidad
            from (
            
            select a.hora_s, a.fecha_s,a.velocidad::integer from observatorio_v2." . $params->table . " a where gid::integer in (select distinct(gid::integer) from (
            select * from observatorio_v2." . $params->tablehex . " h where st_intersects(h.geom,ST_Buffer(st_setsrid(ST_MakePoint(" . $params->lanlng->lng . "," . $params->lanlng->lat . "),4326)::geography,$params->radio))
            )x group by gid) and a.gid!='' and a.velocidad::integer>=10 and a.velocidad::integer<=100 order by 1,2
            
            )y group by y.hora_s,y.velocidad,y.fecha_s order by 1,2,3
            
            )z where z.fecha_s=5  group by z.hora_s,z.fecha_s
            
            )f on hora.hora=f.hora_s::integer left join
            (select z.hora_s, z.fecha_s dia,round(avg(velocidad),0) velo from(
            select 
            y.hora_s,y.fecha_s,y.velocidad
            from (
            
            select a.hora_s, a.fecha_s,a.velocidad::integer from observatorio_v2." . $params->table . " a where gid::integer in (select distinct(gid::integer) from (
            select * from observatorio_v2." . $params->tablehex . " h where st_intersects(h.geom,ST_Buffer(st_setsrid(ST_MakePoint(" . $params->lanlng->lng . "," . $params->lanlng->lat . "),4326)::geography,$params->radio))
            )x group by gid) and a.gid!='' and a.velocidad::integer>=10 and a.velocidad::integer<=100 order by 1,2
            
            )y group by y.hora_s,y.velocidad,y.fecha_s order by 1,2,3
            
            )z where z.fecha_s=6  group by z.hora_s,z.fecha_s
            
            )g on hora.hora=g.hora_s::integer");
            break;
        case 'fs_analisis_det':
            $pg = new PgSql();
            $data = $pg->getRows("select generico
            ,(select count(*) from guru." . $params->table . " where generico=a.generico and anio=2016 and hexagono = (select gid from capas_base.tmp_hex_lima_callao_20201015_1_copia h where st_intersects(h.geom, ST_MakePoint(" . $params->lanlng->lng . ", " . $params->lanlng->lat . ")))) a2016
            ,(select count(*) from guru." . $params->table . " where generico=a.generico and anio=2017 and hexagono = (select gid from capas_base.tmp_hex_lima_callao_20201015_1_copia h where st_intersects(h.geom, ST_MakePoint(" . $params->lanlng->lng . ", " . $params->lanlng->lat . ")))) a2017
            ,(select count(*) from guru." . $params->table . " where generico=a.generico and anio=2018 and hexagono = (select gid from capas_base.tmp_hex_lima_callao_20201015_1_copia h where st_intersects(h.geom, ST_MakePoint(" . $params->lanlng->lng . ", " . $params->lanlng->lat . ")))) a2018
            ,(select count(*) from guru." . $params->table . " where generico=a.generico and anio=2019 and hexagono = (select gid from capas_base.tmp_hex_lima_callao_20201015_1_copia h where st_intersects(h.geom, ST_MakePoint(" . $params->lanlng->lng . ", " . $params->lanlng->lat . ")))) a2019
            ,(select count(*) from guru." . $params->table . " where generico=a.generico and anio=2020 and hexagono = (select gid from capas_base.tmp_hex_lima_callao_20201015_1_copia h where st_intersects(h.geom, ST_MakePoint(" . $params->lanlng->lng . ", " . $params->lanlng->lat . ")))) a2020
            ,(select count(*) from guru." . $params->table . " where generico=a.generico and anio=2021 and hexagono = (select gid from capas_base.tmp_hex_lima_callao_20201015_1_copia h where st_intersects(h.geom, ST_MakePoint(" . $params->lanlng->lng . ", " . $params->lanlng->lat . ")))) a2021
            ,count(*) as cant from guru." . $params->table . " a where hexagono = (select gid from capas_base.tmp_hex_lima_callao_20201015_1_copia h where st_intersects(h.geom, ST_MakePoint(" . $params->lanlng->lng . ", " . $params->lanlng->lat . "))) group by generico order by 2 desc");
            break;
        case 'fs_analisis_det_lal':
            $pg = new PgSql();
            $data = $pg->getRows("
                        select 
                        subcategoria 
                        , count(*) as cant 
                        ,(select count(*) from datacrim.tmp_cruce_trujillo tct where subcategoria = a.subcategoria and anio=2016 and tct.gid=(select 
                            gid 
                            from 
                            datacrim.hex_trujillo h
                            where st_intersects(h.geom, ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . "))) group by subcategoria) a2016
                        ,(select count(*) from datacrim.tmp_cruce_trujillo tct where subcategoria = a.subcategoria and anio=2017 and tct.gid=(select 
                            gid 
                            from 
                            datacrim.hex_trujillo h
                            where st_intersects(h.geom, ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . "))) group by subcategoria) a2017
                        ,(select count(*) from datacrim.tmp_cruce_trujillo tct where subcategoria = a.subcategoria and anio=2018 and tct.gid=(select 
                            gid 
                            from 
                            datacrim.hex_trujillo h
                            where st_intersects(h.geom, ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . "))) group by subcategoria) a2018
                        ,(select count(*) from datacrim.tmp_cruce_trujillo tct where subcategoria = a.subcategoria and anio=2019 and tct.gid=(select 
                            gid 
                            from 
                            datacrim.hex_trujillo h
                            where st_intersects(h.geom, ST_MakePoint(" . $params->lanlng->lng . ", " . $params->lanlng->lat . "))) group by subcategoria) a2019
                        ,(select count(*) from datacrim.tmp_cruce_trujillo tct where subcategoria = a.subcategoria and anio=2020 and tct.gid=(select 
                            gid 
                            from 
                            datacrim.hex_trujillo h
                            where st_intersects(h.geom, ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . "))) group by subcategoria) a2020
                        ,(select count(*) from datacrim.tmp_cruce_trujillo tct where subcategoria = a.subcategoria and anio=2021 and tct.gid=(select 
                            gid 
                            from 
                            datacrim.hex_trujillo h
                            where st_intersects(h.geom, ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . "))) group by subcategoria) a2021
                        ,(select count(*) from datacrim.tmp_cruce_trujillo tct where subcategoria = a.subcategoria and anio=2022 and tct.gid=(select 
                            gid 
                            from 
                            datacrim.hex_trujillo h
                            where st_intersects(h.geom, ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . "))) group by subcategoria) a2022
                            from datacrim.tmp_cruce_trujillo a
                        where gid = (
                            select 
                            gid 
                            from 
                            datacrim.hex_trujillo h
                            where st_intersects(h.geom, ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . "))
                        ) and anio in(2016,2017,2018,2019,2020,2021)
                        group by subcategoria
                        order by 2 desc
            ");
            break;
        case 'fs_analisis_det_aqp':
            $pg = new PgSql();
            $data = $pg->getRows("
                            select 
                            subcategoria 
                            , count(*) as cant 
                            ,(select count(*) from datacrim.tmp_cruce_aqp tct where subcategoria = a.subcategoria and anio=2016 and tct.gid=(select 
                                gid 
                                from 
                                datacrim.hex_aqp_new h
                                where st_intersects(h.geom,  st_setsrid(ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . "),4326))) group by subcategoria) a2016
                            ,(select count(*) from datacrim.tmp_cruce_aqp tct where subcategoria = a.subcategoria and anio=2017 and tct.gid=(select 
                                gid 
                                from 
                                datacrim.hex_aqp_new h
                                where st_intersects(h.geom,  st_setsrid(ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . "),4326))) group by subcategoria) a2017
                            ,(select count(*) from datacrim.tmp_cruce_aqp tct where subcategoria = a.subcategoria and anio=2018 and tct.gid=(select 
                                gid 
                                from 
                                datacrim.hex_aqp_new h
                                where st_intersects(h.geom,  st_setsrid(ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . "),4326))) group by subcategoria) a2018
                            ,(select count(*) from datacrim.tmp_cruce_aqp tct where subcategoria = a.subcategoria and anio=2019 and tct.gid=(select 
                                gid 
                                from 
                                datacrim.hex_aqp_new h
                                where st_intersects(h.geom,  st_setsrid(ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . "),4326))) group by subcategoria) a2019
                            ,(select count(*) from datacrim.tmp_cruce_aqp tct where subcategoria = a.subcategoria and anio=2020 and tct.gid=(select 
                                gid 
                                from 
                                datacrim.hex_aqp_new h
                                where st_intersects(h.geom,  st_setsrid(ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . "),4326))) group by subcategoria) a2020
                            ,(select count(*) from datacrim.tmp_cruce_aqp tct where subcategoria = a.subcategoria and anio=2021 and tct.gid=(select 
                                gid 
                                from 
                                datacrim.hex_aqp_new h
                                where st_intersects(h.geom,  st_setsrid(ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . "),4326))) group by subcategoria) a2021
                            ,(select count(*) from datacrim.tmp_cruce_aqp tct where subcategoria = a.subcategoria and anio=2022 and tct.gid=(select 
                                gid 
                                from 
                                datacrim.hex_aqp_new h
                                where st_intersects(h.geom,  st_setsrid(ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . "),4326))) group by subcategoria) a2022
                                from datacrim.tmp_cruce_aqp a
                            where gid = (
                                select 
                                gid 
                                from 
                                datacrim.hex_aqp_new h
                                where st_intersects(h.geom, st_setsrid(ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . "),4326))
                            ) and anio in(2016,2017,2018,2019,2020,2021)
                            group by subcategoria
                            order by 2 desc
                ");
            break;
            case 'fs_analisis_det_elalto':
                $pg = new PgSql();
                $data = $pg->getRows("
                                select 
                                subcategoria 
                                , count(*) as cant 
                                ,(select count(*) from datacrim.tmp_cruce_elalto tct where subcategoria = a.subcategoria and anio=2016 and tct.gid=(select 
                                    gid 
                                    from 
                                    datacrim.hex_elalto h
                                    where st_intersects(h.geom,  st_setsrid(ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . "),4326))) group by subcategoria) a2016
                                ,(select count(*) from datacrim.tmp_cruce_elalto tct where subcategoria = a.subcategoria and anio=2017 and tct.gid=(select 
                                    gid 
                                    from 
                                    datacrim.hex_elalto h
                                    where st_intersects(h.geom,  st_setsrid(ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . "),4326))) group by subcategoria) a2017
                                ,(select count(*) from datacrim.tmp_cruce_elalto tct where subcategoria = a.subcategoria and anio=2018 and tct.gid=(select 
                                    gid 
                                    from 
                                    datacrim.hex_elalto h
                                    where st_intersects(h.geom,  st_setsrid(ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . "),4326))) group by subcategoria) a2018
                                ,(select count(*) from datacrim.tmp_cruce_elalto tct where subcategoria = a.subcategoria and anio=2019 and tct.gid=(select 
                                    gid 
                                    from 
                                    datacrim.hex_elalto h
                                    where st_intersects(h.geom,  st_setsrid(ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . "),4326))) group by subcategoria) a2019
                                ,(select count(*) from datacrim.tmp_cruce_elalto tct where subcategoria = a.subcategoria and anio=2020 and tct.gid=(select 
                                    gid 
                                    from 
                                    datacrim.hex_elalto h
                                    where st_intersects(h.geom,  st_setsrid(ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . "),4326))) group by subcategoria) a2020
                                ,(select count(*) from datacrim.tmp_cruce_elalto tct where subcategoria = a.subcategoria and anio=2021 and tct.gid=(select 
                                    gid 
                                    from 
                                    datacrim.hex_elalto h
                                    where st_intersects(h.geom,  st_setsrid(ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . "),4326))) group by subcategoria) a2021
                                ,(select count(*) from datacrim.tmp_cruce_elalto tct where subcategoria = a.subcategoria and anio=2022 and tct.gid=(select 
                                    gid 
                                    from 
                                    datacrim.hex_elalto h
                                    where st_intersects(h.geom,  st_setsrid(ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . "),4326))) group by subcategoria) a2022
                                    from datacrim.tmp_cruce_elalto a
                                where gid = (
                                    select 
                                    gid 
                                    from 
                                    datacrim.hex_elalto h
                                    where st_intersects(h.geom, st_setsrid(ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . "),4326))
                                ) and anio in(2016,2017,2018,2019,2020,2021)
                                group by subcategoria
                                order by 2 desc
                    ");
                break;
        case 'fs_analisis_det_lal_radio':
            $pg = new PgSql();
            $data = $pg->getRows("
                            select 
                            subcategoria 
                            , count(*) as cant 
                            ,(select count(*) from datacrim.tmp_cruce_trujillo tct where subcategoria = a.subcategoria and anio=2016 and tct.gid in(select 
                                gid 
                                from 
                                datacrim.hex_trujillo h
                                where st_intersects(h.geom, st_buffer(ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . ")::geography,$params->radio)) and en_hexagono=1) group by subcategoria) a2016
                            ,(select count(*) from datacrim.tmp_cruce_trujillo tct where subcategoria = a.subcategoria and anio=2017 and tct.gid in (select 
                                gid 
                                from 
                                datacrim.hex_trujillo h
                                where st_intersects(h.geom, st_buffer(ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . ")::geography,$params->radio)) and en_hexagono=1) group by subcategoria) a2017
                            ,(select count(*) from datacrim.tmp_cruce_trujillo tct where subcategoria = a.subcategoria and anio=2018 and tct.gid in (select 
                                gid 
                                from 
                                datacrim.hex_trujillo h
                                where st_intersects(h.geom, st_buffer(ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . ")::geography,$params->radio)) and en_hexagono=1) group by subcategoria) a2018
                            ,(select count(*) from datacrim.tmp_cruce_trujillo tct where subcategoria = a.subcategoria and anio=2019 and tct.gid in(select 
                                gid 
                                from 
                                datacrim.hex_trujillo h
                                where st_intersects(h.geom, st_buffer(ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . ")::geography,$params->radio)) and en_hexagono=1) group by subcategoria) a2019
                            ,(select count(*) from datacrim.tmp_cruce_trujillo tct where subcategoria = a.subcategoria and anio=2020 and tct.gid in (select 
                                gid 
                                from 
                                datacrim.hex_trujillo h
                                where st_intersects(h.geom, st_buffer(ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . ")::geography,$params->radio)) and en_hexagono=1) group by subcategoria) a2020
                            ,(select count(*) from datacrim.tmp_cruce_trujillo tct where subcategoria = a.subcategoria and anio=2021 and tct.gid in (select 
                                gid 
                                from 
                                datacrim.hex_trujillo h
                                where st_intersects(h.geom, st_buffer(ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . ")::geography,$params->radio)) and en_hexagono=1) group by subcategoria) a2021
                            ,(select count(*) from datacrim.tmp_cruce_trujillo tct where subcategoria = a.subcategoria and anio=2022 and tct.gid in (select 
                                gid 
                                from 
                                datacrim.hex_trujillo h
                                where st_intersects(h.geom, st_buffer(ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . ")::geography,$params->radio)) and en_hexagono=1) group by subcategoria) a2022
                            from datacrim.tmp_cruce_trujillo a
                            where gid in (
                                select 
                                gid 
                                from 
                                datacrim.hex_trujillo h
                                where st_intersects(h.geom, st_buffer(ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . ")::geography,$params->radio)) and en_hexagono=1
                            ) and anio in(2016,2017,2018,2019,2020,2021)
                            group by subcategoria
                            order by 2 desc
                ");
            break;
        case 'fs_analisis_det_aqp_radio':
            $pg = new PgSql();
            $data = $pg->getRows("
                                select 
                                subcategoria 
                                , count(*) as cant 
                                ,(select count(*) from datacrim.tmp_cruce_aqp tct where subcategoria = a.subcategoria and anio=2016 and tct.gid in(select 
                                    gid 
                                    from 
                                    datacrim.hex_aqp_new h
                                    where st_intersects(h.geom, st_buffer(st_setsrid(ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . "),4326)::geography,$params->radio)) and en_hexagono=1) group by subcategoria) a2016
                                ,(select count(*) from datacrim.tmp_cruce_aqp tct where subcategoria = a.subcategoria and anio=2017 and tct.gid in (select 
                                    gid 
                                    from 
                                    datacrim.hex_aqp_new h
                                    where st_intersects(h.geom, st_buffer(st_setsrid(ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . "),4326)::geography,$params->radio)) and en_hexagono=1) group by subcategoria) a2017
                                ,(select count(*) from datacrim.tmp_cruce_aqp tct where subcategoria = a.subcategoria and anio=2018 and tct.gid in (select 
                                    gid 
                                    from 
                                    datacrim.hex_aqp_new h
                                    where st_intersects(h.geom, st_buffer(st_setsrid(ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . "),4326)::geography,$params->radio)) and en_hexagono=1) group by subcategoria) a2018
                                ,(select count(*) from datacrim.tmp_cruce_aqp tct where subcategoria = a.subcategoria and anio=2019 and tct.gid in(select 
                                    gid 
                                    from 
                                    datacrim.hex_aqp_new h
                                    where st_intersects(h.geom, st_buffer(st_setsrid(ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . "),4326)::geography,$params->radio)) and en_hexagono=1) group by subcategoria) a2019
                                ,(select count(*) from datacrim.tmp_cruce_aqp tct where subcategoria = a.subcategoria and anio=2020 and tct.gid in (select 
                                    gid 
                                    from 
                                    datacrim.hex_aqp_new h
                                    where st_intersects(h.geom, st_buffer(st_setsrid(ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . "),4326)::geography,$params->radio)) and en_hexagono=1) group by subcategoria) a2020
                                ,(select count(*) from datacrim.tmp_cruce_aqp tct where subcategoria = a.subcategoria and anio=2021 and tct.gid in (select 
                                    gid 
                                    from 
                                    datacrim.hex_aqp_new h
                                    where st_intersects(h.geom, st_buffer(st_setsrid(ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . "),4326)::geography,$params->radio)) and en_hexagono=1) group by subcategoria) a2021
                                ,(select count(*) from datacrim.tmp_cruce_aqp tct where subcategoria = a.subcategoria and anio=2022 and tct.gid in (select 
                                    gid 
                                    from 
                                    datacrim.hex_aqp_new h
                                    where st_intersects(h.geom, st_buffer(st_setsrid(ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . "),4326)::geography,$params->radio)) and en_hexagono=1) group by subcategoria) a2022
                                from datacrim.tmp_cruce_aqp a
                                where gid in (
                                    select 
                                    gid 
                                    from 
                                    datacrim.hex_aqp_new h
                                    where st_intersects(h.geom, st_buffer(st_setsrid(ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . "),4326)::geography,$params->radio)) and en_hexagono=1
                                ) and anio in(2016,2017,2018,2019,2020,2021)
                                group by subcategoria
                                order by 2 desc
                    ");
            break;
            case 'fs_analisis_det_elalto_radio':
                $pg = new PgSql();
                $data = $pg->getRows("
                                    select 
                                    subcategoria 
                                    , count(*) as cant 
                                    ,(select count(*) from datacrim.tmp_cruce_elalto tct where subcategoria = a.subcategoria and anio=2016 and tct.gid in(select 
                                        gid 
                                        from 
                                        datacrim.hex_elalto h
                                        where st_intersects(h.geom, st_buffer(st_setsrid(ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . "),4326)::geography,$params->radio)) and en_hexagono=1) group by subcategoria) a2016
                                    ,(select count(*) from datacrim.tmp_cruce_elalto tct where subcategoria = a.subcategoria and anio=2017 and tct.gid in (select 
                                        gid 
                                        from 
                                        datacrim.hex_elalto h
                                        where st_intersects(h.geom, st_buffer(st_setsrid(ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . "),4326)::geography,$params->radio)) and en_hexagono=1) group by subcategoria) a2017
                                    ,(select count(*) from datacrim.tmp_cruce_elalto tct where subcategoria = a.subcategoria and anio=2018 and tct.gid in (select 
                                        gid 
                                        from 
                                        datacrim.hex_elalto h
                                        where st_intersects(h.geom, st_buffer(st_setsrid(ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . "),4326)::geography,$params->radio)) and en_hexagono=1) group by subcategoria) a2018
                                    ,(select count(*) from datacrim.tmp_cruce_elalto tct where subcategoria = a.subcategoria and anio=2019 and tct.gid in(select 
                                        gid 
                                        from 
                                        datacrim.hex_elalto h
                                        where st_intersects(h.geom, st_buffer(st_setsrid(ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . "),4326)::geography,$params->radio)) and en_hexagono=1) group by subcategoria) a2019
                                    ,(select count(*) from datacrim.tmp_cruce_elalto tct where subcategoria = a.subcategoria and anio=2020 and tct.gid in (select 
                                        gid 
                                        from 
                                        datacrim.hex_elalto h
                                        where st_intersects(h.geom, st_buffer(st_setsrid(ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . "),4326)::geography,$params->radio)) and en_hexagono=1) group by subcategoria) a2020
                                    ,(select count(*) from datacrim.tmp_cruce_elalto tct where subcategoria = a.subcategoria and anio=2021 and tct.gid in (select 
                                        gid 
                                        from 
                                        datacrim.hex_elalto h
                                        where st_intersects(h.geom, st_buffer(st_setsrid(ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . "),4326)::geography,$params->radio)) and en_hexagono=1) group by subcategoria) a2021
                                    ,(select count(*) from datacrim.tmp_cruce_elalto tct where subcategoria = a.subcategoria and anio=2022 and tct.gid in (select 
                                        gid 
                                        from 
                                        datacrim.hex_elalto h
                                        where st_intersects(h.geom, st_buffer(st_setsrid(ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . "),4326)::geography,$params->radio)) and en_hexagono=1) group by subcategoria) a2022
                                    from datacrim.tmp_cruce_elalto a
                                    where gid in (
                                        select 
                                        gid 
                                        from 
                                        datacrim.hex_elalto h
                                        where st_intersects(h.geom, st_buffer(st_setsrid(ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . "),4326)::geography,$params->radio)) and en_hexagono=1
                                    ) and anio in(2016,2017,2018,2019,2020,2021)
                                    group by subcategoria
                                    order by 2 desc
                        ");
                break;
    
            case 'fs_analisis_det_pnp':
            $pg = new PgSql();
            $data = $pg->getRows(
                "select 
                    INITCAP(replace(nivel_1,'_',' ')) nivel_1
                    ,count(*) as cant 
                    from guru.delitos_serenos
                    where nivel_0 ='" . $params->where . "' and hexagono in 
                        (select 
                            gid 
                            from capas_base.tmp_hex_lima_callao_pnp h
                            where st_intersects(h.geom,st_buffer( ST_MakePoint(" . $params->lanlng->lng . ", " . $params->lanlng->lat . ")::geography,$params->radio) ))
                    group by 
                    nivel_1
                    order by 1 desc"
            );
            break;
        case 'fs_analisis_det_pnp2':
            $pg = new PgSql();
            $data = $pg->getRows("
                            select 
                            hexagono
                            ,nivel_1 
                            ,count(*) as cant 
                            from guru.delitos_serenos
                            where nivel_0 ='" . $params->where . "' and hexagono = 
                                    (select 
                                        gid 
                                        from capas_base.tmp_hex_lima_callao_pnp h
                                        where st_intersects(h.geom, ST_MakePoint(" . $params->lanlng->lng . ", " . $params->lanlng->lat . "))) 
                                        group by 
                                        hexagono,
                                        nivel_1
                                         order by 1 desc");
            break;
            /**aqui*/
        case 'fs_analisis_det_v2':
            $pg = new PgSql();
            $data = $pg->getRows("select generico, count(*) as cant from guru.tmp_20201020_1 x where st_intersects(x.geom,ST_Buffer(ST_MakePoint(" . $params->lanlng->lng . ", " . $params->lanlng->lat . ")::geography, $params->radio)) and en_hexagono=1 group by generico order by 2 desc");
            break;

            /**aquiitablita niveles */
        case 'fs_analisis_det_tabla_v2':
            $pg = new PgSql();
            $data = $pg->getRows("
            select 
            case 
            when clasifica=1 then 'Muy bajo'
            when clasifica=2 then 'Bajo'	
            when clasifica=3 then 'Medio'	
            when clasifica=4 then 'Alto'	
            when clasifica=5 then 'Muy alto' end clasifica,
            count(*) cant from (
            select hexagono,count(*),clasifica from
            (select gid,clasifica from capas_base.hex_lima_callao h 
            where st_intersects(h.geom, ST_Buffer(ST_MakePoint(" . $params->lanlng->lng . ", " . $params->lanlng->lat . ")::geography,$params->radio)))a inner join (select * from guru.tmp_20201020_1)b on a.gid=b.hexagono 
            group by hexagono,clasifica order by 3
            )x group by clasifica order by clasifica desc               
            ");
            break;
        case 'fs_analisis_det_lal_radio_estado':
            $pg = new PgSql();
            $data = $pg->getRows("
            select 
            case 
            when quintil=1 then 'Muy bajo'
            when quintil=2 then 'Bajo'	
            when quintil=3 then 'Medio'	
            when quintil=4 then 'Alto'	
            when quintil=5 then 'Muy alto' end clasifica,
            count(*) cant from (
            select b.gid,count(*),quintil from
            (select gid,quintil from datacrim.hex_trujillo  h 
            where st_intersects(h.geom, ST_Buffer(ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . ")::geography,$params->radio)) and en_hexagono=1 and quintil>0)a inner join (select * from datacrim.tmp_cruce_trujillo)b on a.gid=b.gid 
            group by b.gid,quintil order by 3
            )x group by quintil order by quintil desc             
            ");
            break;
        case 'fs_analisis_det_aqp_radio_estado':
            $pg = new PgSql();
            $data = $pg->getRows("
            select 
            case 
            when quintil=1 then 'Muy bajo'
            when quintil=2 then 'Bajo'	
            when quintil=3 then 'Medio'	
            when quintil=4 then 'Alto'	
            when quintil=5 then 'Muy alto' end clasifica,
            count(*) cant from (
            select b.gid,count(*),quintil from
            (select gid,quintil from datacrim.hex_aqp_new  h 
            where st_intersects(h.geom, ST_Buffer(st_setsrid(ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . "),4326)::geography,$params->radio)) and en_hexagono=1 and quintil>0)a inner join (select * from datacrim.tmp_cruce_aqp)b on a.gid=b.gid 
            group by b.gid,quintil order by 3
            )x group by quintil order by quintil desc             
            ");
            break;
            case 'fs_analisis_det_elalto_radio_estado':
                $pg = new PgSql();
                $data = $pg->getRows("
                select 
                case 
                when quintil=1 then 'Muy bajo'
                when quintil=2 then 'Bajo'	
                when quintil=3 then 'Medio'	
                when quintil=4 then 'Alto'	
                when quintil=5 then 'Muy alto' end clasifica,
                count(*) cant from (
                select b.gid,count(*),quintil from
                (select gid,quintil from datacrim.hex_elalto  h 
                where st_intersects(h.geom, ST_Buffer(st_setsrid(ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . "),4326)::geography,$params->radio)) and en_hexagono=1 and quintil>0)a inner join (select * from datacrim.tmp_cruce_elalto)b on a.gid=b.gid 
                group by b.gid,quintil order by 3
                )x group by quintil order by quintil desc             
                ");
                break;
        case 'fs_analisis_det_aqp':
            $pg = new PgSql();
            $data = $pg->getRows("select generico, count(*) as cant from guru.tmp_delitos_aqp x where st_intersects(x.geom,ST_Buffer(ST_MakePoint(" . $params->lanlng->lng . ", " . $params->lanlng->lat . ")::geography, $params->radio))  group by generico order by 2 desc");
            break;
        case 'fs_analisis_det_tabla_aqp':
            $pg = new PgSql();
            $data = $pg->getRows("
            select 
            case 
            when clasifica=1 then 'Muy bajo'
            when clasifica=2 then 'Bajo'	
            when clasifica=3 then 'Medio'	
            when clasifica=4 then 'Alto'	
            when clasifica=5 then 'Muy alto' end clasifica,
            count(*) cant from (
            select hexagono,count(*),quintil clasifica from
            (select gid,quintil from datacrim.hex_arequipa h 
            where st_intersects(h.geom, ST_Buffer(ST_MakePoint(" . $params->lanlng->lng . ", " . $params->lanlng->lat . ")::geography,$params->radio)))a inner join (select * from guru.tmp_delitos_aqp)b on a.gid=b.hexagono 
            group by hexagono,quintil order by 3
            )x group by clasifica order by clasifica desc               
            ");
            break;
        case 'fs_analisis_det_tabla_v3':
            $pg = new PgSql();
            $data = $pg->getRows("
            select 
            case 
            when clasifica=1 then 'Muy bajo'
            when clasifica=2 then 'Bajo'	
            when clasifica=3 then 'Medio'	
            when clasifica=4 then 'Alto'	
            when clasifica=5 then 'Muy alto' end clasifica,
            count(*) cant from (
            select a.gid,count(*),clasifica from
            (select gid,clasifica from observatorio_v2." . $params->tablehex . "  h
            where clasifica >0 and st_intersects(h.geom, ST_Buffer(ST_MakePoint(" . $params->lanlng->lng . ", " . $params->lanlng->lat . ")::geography,$params->radio)) order by 1)a
            group by a.gid,clasifica order by 1
            )x group by clasifica order by clasifica desc            
            ");
            break;
            /**fin aqui niveles */
            /**inicio chart v2 */
        case 'fs_analisis_det_chart_v2':
            $pg = new PgSql();
            /*select gid from capas_base.tmp_hex_lima_callao_20201015_1_copia where ST_INTERSECTS(capas_base.tmp_hex_lima_callao_20201015_1_copia.geom, ST_MakePoint(-77.032270431519, -12.102983625557))
			select distrito from distrito_f where ST_INTERSECTS(distrito_f.geom, ST_MakePoint(-77.032270431519, -12.102983625557))*/
            $arr = $pg->getRow("select
             row_to_json(x)
            from(
            select
             (select row_to_json(t) from(select null as \"plotBackgroundColor\", null as \"plotBorderWidth\", false as \"plotShadow\", 'pie' as type) t) as chart,
             (select row_to_json(t) from(select 'Delitos y faltas por microgeocerca'|| ' ubicado en distrito ' || (select distrito from distrito_f where ST_INTERSECTS(distrito_f.geom, ST_MakePoint(" . $params->lanlng->lng . ", " . $params->lanlng->lat . ")))   as text) t) as title,
            (select row_to_json(t) from(select false as enabled) t) as credits,
             (select row_to_json(t) from(select true as enabled) t) as exporting,
			 (select row_to_json(t) from (select row_to_json(t) as pie from(select true as \"showInLegend\") t) t) as \"plotOptions\",
             array_to_json(array_agg(x)) as series
            from(
            select
             'Brands' as name,
             true as \"colorByPoint\",
             array_to_json(array_agg(x)) as data
            from(
                select generico as name, count(*) as y from guru.tmp_20201020_1 x where st_intersects(x.geom,ST_Buffer(ST_MakePoint(" . $params->lanlng->lng . ", " . $params->lanlng->lat . ")::geography, $params->radio)) and en_hexagono=1 group by generico order by 2 desc
            ) x
            ) x
            ) x");
            $data = $arr->row_to_json;
            break;
            /**fin chart v2 */

        case 'fs_analisis_det_chart_v2_aqp':
            $pg = new PgSql();
            /*select gid from capas_base.tmp_hex_lima_callao_20201015_1_copia where ST_INTERSECTS(capas_base.tmp_hex_lima_callao_20201015_1_copia.geom, ST_MakePoint(-77.032270431519, -12.102983625557))
                select distrito from distrito_f where ST_INTERSECTS(distrito_f.geom, ST_MakePoint(-77.032270431519, -12.102983625557))*/
            $arr = $pg->getRow("select
                 row_to_json(x)
                from(
                select
                 (select row_to_json(t) from(select null as \"plotBackgroundColor\", null as \"plotBorderWidth\", false as \"plotShadow\", 'pie' as type) t) as chart,
                 (select row_to_json(t) from(select 'Delitos y faltas por microgeocerca'::text as text) t) as title,
                (select row_to_json(t) from(select false as enabled) t) as credits,
                 (select row_to_json(t) from(select true as enabled) t) as exporting,
                 (select row_to_json(t) from (select row_to_json(t) as pie from(select true as \"showInLegend\") t) t) as \"plotOptions\",
                 array_to_json(array_agg(x)) as series
                from(
                select
                 'Brands' as name,
                 true as \"colorByPoint\",
                 array_to_json(array_agg(x)) as data
                from(
                    select generico as name, count(*) as y from guru.tmp_delitos_aqp x where st_intersects(x.geom,ST_Buffer(ST_MakePoint(" . $params->lanlng->lng . ", " . $params->lanlng->lat . ")::geography, $params->radio))  group by generico order by 2 desc
                ) x
                ) x
                ) x");
            $data = $arr->row_to_json;
            break;
            /**fin chart v2 */
        case 'fs_analisis_det_chart':
            $pg = new PgSql();
            /*select gid from capas_base.tmp_hex_lima_callao_20201015_1_copia where ST_INTERSECTS(capas_base.tmp_hex_lima_callao_20201015_1_copia.geom, ST_MakePoint(-77.032270431519, -12.102983625557))
			select distrito from distrito_f where ST_INTERSECTS(distrito_f.geom, ST_MakePoint(-77.032270431519, -12.102983625557))*/
            $arr = $pg->getRow("select
             row_to_json(x)
            from(
            select
             (select row_to_json(t) from(select null as \"plotBackgroundColor\", null as \"plotBorderWidth\", false as \"plotShadow\", 'pie' as type) t) as chart,
             (select row_to_json(t) from(select 'Denuncias PNP por Geocerca #' || (select gid from capas_base.tmp_hex_lima_callao_20201015_1_copia where ST_INTERSECTS(capas_base.tmp_hex_lima_callao_20201015_1_copia.geom, ST_MakePoint(" . $params->lanlng->lng . ", " . $params->lanlng->lat . "))) || ' ubicado en distrito ' || (select distrito from distrito_f where ST_INTERSECTS(distrito_f.geom, ST_MakePoint(" . $params->lanlng->lng . ", " . $params->lanlng->lat . ")))   as text) t) as title,
             (select row_to_json(t) from(select false as enabled) t) as credits,
             (select row_to_json(t) from(select true as enabled) t) as exporting,
			 (select row_to_json(t) from (select row_to_json(t) as pie from(select true as \"showInLegend\") t) t) as \"plotOptions\",
             array_to_json(array_agg(x)) as series
            from(
            select
             'Brands' as name,
             true as \"colorByPoint\",
             array_to_json(array_agg(x)) as data
            from(
            select 
             generico as name, 
             count(*) as y 
            from guru." . $params->table . " 
            where 
             hexagono = (select gid from capas_base.tmp_hex_lima_callao_20201015_1_copia h where st_intersects(h.geom, ST_MakePoint(" . $params->lanlng->lng . ", " . $params->lanlng->lat . "))) 
            group by 
             generico 
            order by 
             2 desc
            ) x
            ) x
            ) x");
            $data = $arr->row_to_json;
            break;
        case 'fs_analisis_det_lal_chart':
            $pg = new PgSql();
            /*select gid from capas_base.tmp_hex_lima_callao_20201015_1_copia where ST_INTERSECTS(capas_base.tmp_hex_lima_callao_20201015_1_copia.geom, ST_MakePoint(-77.032270431519, -12.102983625557))
                select distrito from distrito_f where ST_INTERSECTS(distrito_f.geom, ST_MakePoint(-77.032270431519, -12.102983625557))*/
            $arr = $pg->getRow("select
                 row_to_json(x)
                from(
                select
                 (select row_to_json(t) from(select null as \"plotBackgroundColor\", null as \"plotBorderWidth\", false as \"plotShadow\", 'pie' as type) t) as chart,
                 (select row_to_json(t) from(select 'Delitos y faltas por microgeocerca #' || (select gid from datacrim.hex_trujillo where ST_INTERSECTS(datacrim.hex_trujillo.geom, ST_MakePoint(" . $params->lanlng->lng . ", " . $params->lanlng->lat . "))) || ' ubicado en distrito ' || (select distrito from distrito_f where ST_INTERSECTS(distrito_f.geom, ST_MakePoint(" . $params->lanlng->lng . ", " . $params->lanlng->lat . ")))   as text) t) as title,
                 (select row_to_json(t) from(select false as enabled) t) as credits,
                 (select row_to_json(t) from(select true as enabled) t) as exporting,
                 (select row_to_json(t) from (select row_to_json(t) as pie from(select true as \"showInLegend\") t) t) as \"plotOptions\",
                 array_to_json(array_agg(x)) as series
                from(
                select
                 'Brands' as name,
                 true as \"colorByPoint\",
                 array_to_json(array_agg(x)) as data
                from(
                    select 
                    subcategoria as name
                    --,case when anio=2016 then count(*) end a2016 
                    , count(*) as y
                    from datacrim.tmp_cruce_trujillo a
                    where gid = (
                        select 
                        gid 
                        from 
                        datacrim.hex_trujillo h
                        where st_intersects(h.geom, ST_MakePoint(" . $params->lanlng->lng . ", " . $params->lanlng->lat . "))
                    ) and  anio in(2016,2017,2018,2019,2020,2021)
                    group by subcategoria
                    order by 2 desc                
                ) x
                ) x
                ) x");
            $data = $arr->row_to_json;
            break;
        case 'fs_analisis_det_lal_radio_chart':
            $pg = new PgSql();
            /*select gid from capas_base.tmp_hex_lima_callao_20201015_1_copia where ST_INTERSECTS(capas_base.tmp_hex_lima_callao_20201015_1_copia.geom, ST_MakePoint(-77.032270431519, -12.102983625557))
                select distrito from distrito_f where ST_INTERSECTS(distrito_f.geom, ST_MakePoint(-77.032270431519, -12.102983625557))*/
            $arr = $pg->getRow("select
                 row_to_json(x)
                from(
                select
                 (select row_to_json(t) from(select null as \"plotBackgroundColor\", null as \"plotBorderWidth\", false as \"plotShadow\", 'pie' as type) t) as chart,
                 (select row_to_json(t) from(select 'Delitos y faltas por microgeocerca #' || (select gid from datacrim.hex_trujillo where ST_INTERSECTS(datacrim.hex_trujillo.geom, ST_MakePoint(" . $params->lanlng->lng . ", " . $params->lanlng->lat . "))) || ' ubicado en distrito ' || (select distrito from distrito_f where ST_INTERSECTS(distrito_f.geom, ST_MakePoint(" . $params->lanlng->lng . ", " . $params->lanlng->lat . ")))   as text) t) as title,
                 (select row_to_json(t) from(select false as enabled) t) as credits,
                 (select row_to_json(t) from(select true as enabled) t) as exporting,
                 (select row_to_json(t) from (select row_to_json(t) as pie from(select true as \"showInLegend\") t) t) as \"plotOptions\",
                 array_to_json(array_agg(x)) as series
                from(
                select
                 'Brands' as name,
                 true as \"colorByPoint\",
                 array_to_json(array_agg(x)) as data
                from(
                    select 
                    subcategoria as name
                    --,case when anio=2016 then count(*) end a2016 
                    , count(*) as y
                    from datacrim.tmp_cruce_trujillo a
                    where gid in (
                        select 
                        gid 
                        from 
                        datacrim.hex_trujillo h
                        where st_intersects(h.geom, st_buffer(ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . ")::geography,$params->radio)) and en_hexagono=1
                    ) and  anio in(2016,2017,2018,2019,2020,2021) 
                    group by subcategoria
                    order by 2 desc             
                ) x
                ) x
                ) x");
            $data = $arr->row_to_json;
            break;
        case 'fs_analisis_det_aqp_chart':
            $pg = new PgSql();
            /*select gid from capas_base.tmp_hex_lima_callao_20201015_1_copia where ST_INTERSECTS(capas_base.tmp_hex_lima_callao_20201015_1_copia.geom, ST_MakePoint(-77.032270431519, -12.102983625557))
                    select distrito from distrito_f where ST_INTERSECTS(distrito_f.geom, ST_MakePoint(-77.032270431519, -12.102983625557))*/
            $arr = $pg->getRow("select
                     row_to_json(x)
                    from(
                    select
                     (select row_to_json(t) from(select null as \"plotBackgroundColor\", null as \"plotBorderWidth\", false as \"plotShadow\", 'pie' as type) t) as chart,
                     (select row_to_json(t) from(select 'Delitos y faltas por microgeocerca #' || (select gid from datacrim.hex_aqp_new where ST_INTERSECTS(datacrim.hex_aqp_new.geom, st_setsrid(ST_MakePoint(" . $params->lanlng->lng . ", " . $params->lanlng->lat . "),4326))) || ' ubicado en distrito ' || (select distrito from distrito_f where ST_INTERSECTS(distrito_f.geom, ST_MakePoint(" . $params->lanlng->lng . ", " . $params->lanlng->lat . ")))   as text) t) as title,
                     (select row_to_json(t) from(select false as enabled) t) as credits,
                     (select row_to_json(t) from(select true as enabled) t) as exporting,
                     (select row_to_json(t) from (select row_to_json(t) as pie from(select true as \"showInLegend\") t) t) as \"plotOptions\",
                     array_to_json(array_agg(x)) as series
                    from(
                    select
                     'Brands' as name,
                     true as \"colorByPoint\",
                     array_to_json(array_agg(x)) as data
                    from(
                        select 
                        subcategoria as name
                        --,case when anio=2016 then count(*) end a2016 
                        , count(*) as y
                        from datacrim.tmp_cruce_aqp a
                        where gid = (
                            select 
                            gid 
                            from 
                            datacrim.hex_aqp_new h
                            where st_intersects(h.geom, st_setsrid(ST_MakePoint(" . $params->lanlng->lng . ", " . $params->lanlng->lat . "),4326))
                        ) and  anio in(2016,2017,2018,2019,2020,2021)
                        group by subcategoria
                        order by 2 desc                
                    ) x
                    ) x
                    ) x");
            $data = $arr->row_to_json;
            break;
        case 'fs_analisis_det_elalto_chart':
            $pg = new PgSql();
            /*select gid from capas_base.tmp_hex_lima_callao_20201015_1_copia where ST_INTERSECTS(capas_base.tmp_hex_lima_callao_20201015_1_copia.geom, ST_MakePoint(-77.032270431519, -12.102983625557))
                    select distrito from distrito_f where ST_INTERSECTS(distrito_f.geom, ST_MakePoint(-77.032270431519, -12.102983625557))*/
            $arr = $pg->getRow("select
                     row_to_json(x)
                    from(
                    select
                     (select row_to_json(t) from(select null as \"plotBackgroundColor\", null as \"plotBorderWidth\", false as \"plotShadow\", 'pie' as type) t) as chart,
                     (select row_to_json(t) from(select 'Delitos y faltas por microgeocerca #' || (select gid from datacrim.hex_elalto where ST_INTERSECTS(datacrim.hex_elalto.geom, st_setsrid(ST_MakePoint(" . $params->lanlng->lng . ", " . $params->lanlng->lat . "),4326))) || ' ubicado en distrito ' || (select distrito from distrito_f where ST_INTERSECTS(distrito_f.geom, ST_MakePoint(" . $params->lanlng->lng . ", " . $params->lanlng->lat . ")))   as text) t) as title,
                     (select row_to_json(t) from(select false as enabled) t) as credits,
                     (select row_to_json(t) from(select true as enabled) t) as exporting,
                     (select row_to_json(t) from (select row_to_json(t) as pie from(select true as \"showInLegend\") t) t) as \"plotOptions\",
                     array_to_json(array_agg(x)) as series
                    from(
                    select
                     'Brands' as name,
                     true as \"colorByPoint\",
                     array_to_json(array_agg(x)) as data
                    from(
                        select 
                        subcategoria as name
                        --,case when anio=2016 then count(*) end a2016 
                        , count(*) as y
                        from datacrim.tmp_cruce_elalto a
                        where gid = (
                            select 
                            gid 
                            from 
                            datacrim.hex_elalto h
                            where st_intersects(h.geom, st_setsrid(ST_MakePoint(" . $params->lanlng->lng . ", " . $params->lanlng->lat . "),4326))
                        ) and  anio in(2016,2017,2018,2019,2020,2021)
                        group by subcategoria
                        order by 2 desc                
                    ) x
                    ) x
                    ) x");
            $data = $arr->row_to_json;
            break;
        case 'fs_analisis_det_aqp_radio_chart':
            $pg = new PgSql();
            /*select gid from capas_base.tmp_hex_lima_callao_fs_analisis_det_aqp_chart20201015_1_copia where ST_INTERSECTS(capas_base.tmp_hex_lima_callao_20201015_1_copia.geom, ST_MakePoint(-77.032270431519, -12.102983625557))
                    select distrito from distrito_f where ST_INTERSECTS(distrito_f.geom, ST_MakePoint(-77.032270431519, -12.102983625557))*/
            $arr = $pg->getRow("select
                     row_to_json(x)
                    from(
                    select
                     (select row_to_json(t) from(select null as \"plotBackgroundColor\", null as \"plotBorderWidth\", false as \"plotShadow\", 'pie' as type) t) as chart,
                     (select row_to_json(t) from(select 'Delitos y faltas por microgeocerca #' || (select gid from datacrim.hex_aqp_new where ST_INTERSECTS(datacrim.hex_aqp_new.geom, st_setsrid(ST_MakePoint(" . $params->lanlng->lng . ", " . $params->lanlng->lat . "),4326))) || ' ubicado en distrito ' || (select distrito from distrito_f where ST_INTERSECTS(distrito_f.geom, ST_MakePoint(" . $params->lanlng->lng . ", " . $params->lanlng->lat . ")))   as text) t) as title,
                     (select row_to_json(t) from(select false as enabled) t) as credits,
                     (select row_to_json(t) from(select true as enabled) t) as exporting,
                     (select row_to_json(t) from (select row_to_json(t) as pie from(select true as \"showInLegend\") t) t) as \"plotOptions\",
                     array_to_json(array_agg(x)) as series
                    from(
                    select
                     'Brands' as name,
                     true as \"colorByPoint\",
                     array_to_json(array_agg(x)) as data
                    from(
                        select 
                        subcategoria as name
                        --,case when anio=2016 then count(*) end a2016 
                        , count(*) as y
                        from datacrim.tmp_cruce_aqp a
                        where gid in (
                            select 
                            gid 
                            from 
                            datacrim.hex_aqp_new h
                            where st_intersects(h.geom, st_buffer(st_setsrid(ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . "),4326)::geography,$params->radio)) and en_hexagono=1
                        ) and  anio in(2016,2017,2018,2019,2020,2021) 
                        group by subcategoria
                        order by 2 desc             
                    ) x
                    ) x
                    ) x");
            $data = $arr->row_to_json;
            break;
        case 'fs_analisis_det_elalto_radio_chart':
            $pg = new PgSql();
            /*select gid from capas_base.tmp_hex_lima_callao_fs_analisis_det_aqp_chart20201015_1_copia where ST_INTERSECTS(capas_base.tmp_hex_lima_callao_20201015_1_copia.geom, ST_MakePoint(-77.032270431519, -12.102983625557))
                    select distrito from distrito_f where ST_INTERSECTS(distrito_f.geom, ST_MakePoint(-77.032270431519, -12.102983625557))*/
            $arr = $pg->getRow("select
                     row_to_json(x)
                    from(
                    select
                     (select row_to_json(t) from(select null as \"plotBackgroundColor\", null as \"plotBorderWidth\", false as \"plotShadow\", 'pie' as type) t) as chart,
                     (select row_to_json(t) from(select 'Delitos y faltas por microgeocerca #' || (select gid from datacrim.hex_elalto where ST_INTERSECTS(datacrim.hex_elalto.geom, st_setsrid(ST_MakePoint(" . $params->lanlng->lng . ", " . $params->lanlng->lat . "),4326))) || ' ubicado en distrito ' || (select distrito from distrito_f where ST_INTERSECTS(distrito_f.geom, ST_MakePoint(" . $params->lanlng->lng . ", " . $params->lanlng->lat . ")))   as text) t) as title,
                     (select row_to_json(t) from(select false as enabled) t) as credits,
                     (select row_to_json(t) from(select true as enabled) t) as exporting,
                     (select row_to_json(t) from (select row_to_json(t) as pie from(select true as \"showInLegend\") t) t) as \"plotOptions\",
                     array_to_json(array_agg(x)) as series
                    from(
                    select
                     'Brands' as name,
                     true as \"colorByPoint\",
                     array_to_json(array_agg(x)) as data
                    from(
                        select 
                        subcategoria as name
                        --,case when anio=2016 then count(*) end a2016 
                        , count(*) as y
                        from datacrim.tmp_cruce_elalto a
                        where gid in (
                            select 
                            gid 
                            from 
                            datacrim.hex_elalto h
                            where st_intersects(h.geom, st_buffer(st_setsrid(ST_MakePoint( " . $params->lanlng->lng . ", " . $params->lanlng->lat . "),4326)::geography,$params->radio)) and en_hexagono=1
                        ) and  anio in(2016,2017,2018,2019,2020,2021) 
                        group by subcategoria
                        order by 2 desc             
                    ) x
                    ) x
                    ) x");
            $data = $arr->row_to_json;
            break;

        case 'fs_nom_dist':
            $pg = new PgSql();
            $data = $pg->getRows("select iddist as id, distrito as nombre from distrito_f where st_intersects(distrito_f.geom, ST_MakePoint(" . $params->lanlng->lng . ", " . $params->lanlng->lat . "))");
            break;
        case 'fs_mapa_calor':
            $pg = new PgSql();
            $data = $pg->getRows("select latitud, longitud from guru." . $params->table . " d where iddist = '" . $params->iddist . "'");
            break;
        case 'fs_hexagonos_prov_graf':
            $pg = new PgSql();
            $query = "select
                x.*,
                muy_bajo + bajo + medio + alto + muy_alto as total
                from(
                select
                nombprov as valor,
                SUM( CASE WHEN clasifica = 1 THEN 1 ELSE 0 END )  as muy_bajo,
                SUM( CASE WHEN clasifica = 2 THEN 1 ELSE 0 END )  as bajo,
                SUM( CASE WHEN clasifica = 3 THEN 1 ELSE 0 END )  as medio,
                SUM( CASE WHEN clasifica = 4 THEN 1 ELSE 0 END )  as alto,
                SUM( CASE WHEN clasifica = 5 THEN 1 ELSE 0 END )  as muy_alto
                from provincia p
                inner join capas_base.tmp_hex_lima_callao_20201015_1_copia h on substr(h.iddist,1,4) = p.first_idpr
                where
                first_idpr in('0701', '1501')
                group by
                first_idpr,
                nombprov
                order by
                2
                ) x";

            $arr = $pg->getRow("select
                        row_to_json(x)
                       FROM(
                        select
                         (select row_to_json(t) from(select 'column' as type) t) as chart,
                         (select row_to_json(t) from(select 'Total de hexágonos por provincias' as text) t) as title,
                         (select row_to_json(t) from(select false as enabled) t) as credits,
                         (select Row_to_json(t) FROM(select json_agg(x.valor) AS categories) t) AS \"xAxis\",
                         (select row_to_json(t) from (select row_to_json(t) as title from(select 'Cantidad de hexágonos' as text) t) t) as \"yAxis\",
                         (select row_to_json(t) from(select true as enabled) t) as exporting,
                         (
                          select
                           array_to_json(array_agg(x)) as series
                          from(
                           select 'Muy alto' as name, JSON_AGG(x.muy_alto) as data, '#8C0000' as color union all
                           select 'Alto' as name, JSON_AGG(x.alto) as data, '#D90000' as color union all
                           select 'Medio' as name, JSON_AGG(x.medio) as data, '#FF5C26' as color union all
                           select 'Bajo' as name, JSON_AGG(x.bajo) as data, '#FF7F00' as color union all
                           select 'Muy bajo' as name, JSON_AGG(x.muy_bajo) as data, '#FFC926' as color
                          ) x
                         )
                        from(
                         " . $query . "
                        ) x
                       ) x");

            $data = $arr->row_to_json;
            //$data = $pg->getRows("select * from capas_base.tmp_hex_prov");
            /*$data = $pg->getRows("select  id as codubigeo, 'Provincia' as ubigeo, 0 as muy_alto, 0 as alto, 0 as medio, 0 as bajo, 0 as muy_bajo, 0 as total from generate_series(1,4) as id");*/
            break;
        case 'fs_hexagonos_vias_graf':
            $pg = new PgSql();
            $query = "select
            x.*,
            muy_bajo + bajo + medio + alto + muy_alto as total
           from(
           select
            case
                when id_via = 1 then 'cvnacional'
                when id_via = 2 then 'cvmetro'
                when id_via = 3 then 'cvarterial'
                when id_via = 4 then 'cvcolector'
                when id_via = 5 then 'ccanalizad'
                when id_via = 6 then 'cfibra'
              end as cod_via,
              case
                when id_via = 1 then 'Nacional'
                when id_via = 2 then 'Metropolitana'
                when id_via = 3 then 'Arterial'
                when id_via = 4 then 'Colectora'
                when id_via = 5 then 'Canalizado'
                when id_via = 6 then 'Fibra'
              end as valor,
              case
                when id_via = 1 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cvnacional = 1 and clasifica = 1)
                when id_via = 2 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cvmetro = 1 and clasifica = 1)
                when id_via = 3 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cvarterial = 1 and clasifica = 1)
                when id_via = 4 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cvcolector = 1 and clasifica = 1)
                when id_via = 5 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where ccanalizado = 1 and clasifica = 1)
                when id_via = 6 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cfibra = 1 and clasifica = 1)
              end as muy_bajo,
              case
                when id_via = 1 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cvnacional = 1 and clasifica = 2)
                when id_via = 2 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cvmetro = 1 and clasifica = 2)
                when id_via = 3 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cvarterial = 1 and clasifica = 2)
                when id_via = 4 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cvcolector = 1 and clasifica = 2)
                when id_via = 5 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where ccanalizado = 1 and clasifica = 2)
                when id_via = 6 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cfibra = 1 and clasifica = 2)
              end as bajo,
              case
                when id_via = 1 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cvnacional = 1 and clasifica = 3)
                when id_via = 2 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cvmetro = 1 and clasifica = 3)
                when id_via = 3 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cvarterial = 1 and clasifica = 3)
                when id_via = 4 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cvcolector = 1 and clasifica = 3)
                when id_via = 5 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where ccanalizado = 1 and clasifica = 3)
                when id_via = 6 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cfibra = 1 and clasifica = 3)
              end as medio,
              case
                when id_via = 1 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cvnacional = 1 and clasifica = 4)
                when id_via = 2 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cvmetro = 1 and clasifica = 4)
                when id_via = 3 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cvarterial = 1 and clasifica = 4)
                when id_via = 4 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cvcolector = 1 and clasifica = 4)
                when id_via = 5 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where ccanalizado = 1 and clasifica = 4)
                when id_via = 6 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cfibra = 1 and clasifica = 4)
              end as alto,
              case
                when id_via = 1 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cvnacional = 1 and clasifica = 5)
                when id_via = 2 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cvmetro = 1 and clasifica = 5)
                when id_via = 3 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cvarterial = 1 and clasifica = 5)
                when id_via = 4 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cvcolector = 1 and clasifica = 5)
                when id_via = 5 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where ccanalizado = 1 and clasifica = 5)
                when id_via = 6 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cfibra = 1 and clasifica = 5)
              end as muy_alto
           from generate_series(1,4) as id_via
           ) x";

            $arr = $pg->getRow("select
                        row_to_json(x)
                       FROM(
                        select
                         (select row_to_json(t) from(select 'column' as type) t) as chart,
                         (select row_to_json(t) from(select 'Total de hexágonos por vías' as text) t) as title,
                         (select row_to_json(t) from(select false as enabled) t) as credits,
                         (select Row_to_json(t) FROM(select json_agg(x.valor) AS categories) t) AS \"xAxis\",
                         (select row_to_json(t) from (select row_to_json(t) as title from(select 'Cantidad de hexágonos' as text) t) t) as \"yAxis\",
                         (select row_to_json(t) from(select true as enabled) t) as exporting,
                         (
                          select
                           array_to_json(array_agg(x)) as series
                          from(
                            select 'Muy alto' as name, JSON_AGG(x.muy_alto) as data, '#8C0000' as color union all
                            select 'Alto' as name, JSON_AGG(x.alto) as data, '#D90000' as color union all
                            select 'Medio' as name, JSON_AGG(x.medio) as data, '#FF5C26' as color union all
                            select 'Bajo' as name, JSON_AGG(x.bajo) as data, '#FF7F00' as color union all
                            select 'Muy bajo' as name, JSON_AGG(x.muy_bajo) as data, '#FFC926' as color
                          ) x
                         )
                        from(
                         " . $query . "
                        ) x
                       ) x");

            $data = $arr->row_to_json;
            break;
        case 'fs_hexagonos_operador_graf':
            $pg = new PgSql();
            $query = "select
                x.*,
                muy_bajo + bajo + medio + alto + muy_alto as total
               from(
               select
                case
                    when id_via = 1 then 'cvnacional'
                    when id_via = 2 then 'cvmetro'
                    when id_via = 3 then 'cvarterial'
                    when id_via = 4 then 'cvcolector'
                    when id_via = 5 then 'ccanalizad'
                    when id_via = 6 then 'cfibra'
                  end as cod_via,
                  case
                    when id_via = 1 then 'Canalizado'
                    when id_via = 2 then 'Fibra'
                  end as valor,
                  case
                    when id_via = 1 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where ccanalizado = 1 and clasifica = 1)
                    when id_via = 2 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cfibra = 1 and clasifica = 1)
                  end as muy_bajo,
                  case
                    when id_via = 1 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where ccanalizado = 1 and clasifica = 2)
                    when id_via = 2 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cfibra = 1 and clasifica = 2)
                  end as bajo,
                  case
                    when id_via = 1 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where ccanalizado = 1 and clasifica = 3)
                    when id_via = 2 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cfibra = 1 and clasifica = 3)
                  end as medio,
                  case
                    when id_via = 1 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where ccanalizado = 1 and clasifica = 4)
                    when id_via = 2 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cfibra = 1 and clasifica = 4)
                  end as alto,
                  case
                    when id_via = 1 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where ccanalizado = 1 and clasifica = 5)
                    when id_via = 2 then (select count(*) from capas_base.tmp_hex_lima_callao_20201015_1_copia where cfibra = 1 and clasifica = 5)
                  end as muy_alto
               from generate_series(1,2) as id_via
               ) x";

            $arr = $pg->getRow("select
                            row_to_json(x)
                           FROM(
                            select
                             (select row_to_json(t) from(select 'column' as type) t) as chart,
                             (select row_to_json(t) from(select 'Total de hexágonos por operador' as text) t) as title,
                             (select row_to_json(t) from(select false as enabled) t) as credits,
                             (select Row_to_json(t) FROM(select json_agg(x.valor) AS categories) t) AS \"xAxis\",
                             (select row_to_json(t) from (select row_to_json(t) as title from(select 'Cantidad de hexágonos' as text) t) t) as \"yAxis\",
                             (select row_to_json(t) from(select true as enabled) t) as exporting,
                             (
                              select
                               array_to_json(array_agg(x)) as series
                              from(
                                select 'Muy alto' as name, JSON_AGG(x.muy_alto) as data, '#8C0000' as color union all
                                select 'Alto' as name, JSON_AGG(x.alto) as data, '#D90000' as color union all
                                select 'Medio' as name, JSON_AGG(x.medio) as data, '#FF5C26' as color union all
                                select 'Bajo' as name, JSON_AGG(x.bajo) as data, '#FF7F00' as color union all
                                select 'Muy bajo' as name, JSON_AGG(x.muy_bajo) as data, '#FFC926' as color
                              ) x
                             )
                            from(
                             " . $query . "
                            ) x
                           ) x");

            $data = $arr->row_to_json;
            break;
    }
    return $data;
}
